Dactylo Beater


1. INTRODUCTION

Durant cette période de confinement, de nombreuses personnes aiment se lancer des défis. L’un des défis qui est très intéressant est la dactylographie. C’est pourquoi nous avons créé un jeu appelé Dactylo Beater, un jeu dans lequel les joueurs doivent taper le plus rapidement possible des mots ou des phrases choisis par l’algorithme du jeu. 

Le jeu aura un système de score qui évoluera au fur et à mesure de la performance des joueurs qui se concentre sur la rapidité mais aussi la précision du joueur. Le principal objectif de cette application est de permettre aux utilisateurs d’ordinateurs de manier leur clavier rapidement et efficacement tout en s’amusant. 

Les joueurs pourront défier leurs amis en mode multi-joueurs et pourraient voir en option l’évolution de leur performance au cours du temps.
 

2. CONCEPTION

Technologies utilisées:

Partie Back-End: 

Pour la partie Back-End, nous utiliserons du Node JS afin d’envoyer toutes les données comme les identifiants des joueurs ou encore les scores qu’ils ont obtenus. Le node JS sera placé dans un dossier “server”, où nous mettrons toutes l’ API GET, POST, PUT et DELETE. 

Partie Front-End: React

Le React nous permettra de réaliser la partie interface et d’afficher les éléments que nous avons eu à partir de la base de données. Il sera facile pour nous d'intégrer le HTML et le CSS. Le code en react sera placé dans le dossier “client” du projet.

Base de donnée: MySQL

Nous avons choisi cette base de données assez facile à déployer sur certains hébergeurs gratuits comme awseducate.

Websocket (socket.io)

Nous avons opté pour l’utilisation d’un websocket pour la gestion de plusieurs joueurs connectés simultanément. Cela permettra également aux joueurs de taper en même temps sur leur clavier pour envoyer la réponse. Plus précisément, nous utiliserons socket.io.


FEUILLE DE ROUTE DU PROJET


- Sprint 1 : Initialisation (1 Semaine)
initialisation du projet côté frontend et backend. Création des pages du site web et création de la database.
Création de l’authentification 
Création de l’API

- Sprint 2 : Création du jeu v1  (1 Semaine)
Création de la fonctionnalité du jeu : affichage d’un mot, input utilisateur et envoi de celle-ci au backend.
Création du système de score et système de high score sur la page “High Score” entre tous les joueurs inscrits
Graphique d’évolution du joueur

- Sprint 3 : Amélioration du gameplay + mise en ligne  (1 Semaine)
Introduction et mise en place du socket pour la partie multijoeur côté serveur
Sécurisation du client et du serveur en HTTPS grâce à Let's Encrypt
Mise en ligne du site

- Sprint 4 : Jeu Complet  (1 Semaine +)
Implémentation du multijoueur côté client
Gestion du déploiement du site internet
Correction de bugs


3. CHARGE DE TRAVAIL
	
Nicolas THONG: Responsable du backend, création de la database et web service
Julien QUACH : Responsable Frontend, création du site web avec React et création des fonctionnalités du jeu vidéo
William KANN: Fullstack, création frontend, backend
Anil DEVADAS : Fullstack, création frontend, backend
Yann AQUILON : Fullstack, création frontend, backend


CALENDRIER PREVISIONNEL 

Lundi 9 Novembre :  Début du Sprint 1, initialisation du projet et première réunion.

Samedi 14 Novembre : Point d’avancement, pair programming, rétrospective du sprint.

Lundi 16 Novembre :  Début du Sprint 2, création des premières fonctionnalités du jeu et deuxième réunion.

Samedi 21 Novembre : Point d’avancement, pair programming, rétrospective du sprint.

Lundi 23 Novembre :  Début du Sprint 3, amélioration du gameplay et mise en ligne du site internet . Troisième réunion.

Samedi 28 Novembre : Point d’avancement, pair programming, rétrospective du sprint.

Lundi 30 Novembre :  Début du Sprint 4, création du multijoueur, et gestion du déploiement du site internet. Quatrième réunion

Samedi 5 Décembre : Point d’avancement, pair programming, rétrospective du sprint.

Lundi 7 Décembre :  Réunion avancement du projet. Pair programming sur tous les bloquants.

Samedi 12 Décembre : Dernière réunion, point avancement et préparation à la soutenance.


4. OBJECTIF DU PROJET INITIAL

Must Have : 
Jeu solo
le joueur doit écrire le plus vite possible les mots qui s’affichent à l’écran 
Possibilité d'ajouter un mot par l'utilisateur (5 points maximum)
Ajout d’un tableau de score
Login, connexion de l’utilisateur avec son pseudo et mot de passe
score = accumulation des points 


Should Have :
Jeu solo
Le joueur doit écrire le plus vite possible les mots qui s’affichent à l’écran
score = accumulation des points + indicateur de précision (mots incorrectes écrits/total) 


Nice to Have :
Jeu multijoueur avec socket.io
score = premier joueur à avoir atteint les 100 points
Evolution des performances du joueur sous la forme d’un graphe
Système de liste d’amis
Création de multiple rooms multijoeur + Invitation à la room (sécurisé mot de passe)
Création de plusieurs mode de jeu (textes, images, thèmes, mémorisation)







