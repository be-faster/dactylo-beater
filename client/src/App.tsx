import React from "react";
import { Route, Switch } from "react-router-dom";
import NavBar from "./components/header/NavBar";
import Home from "./pages/home/Home";
import Login from "./pages/login/Login";
import Profile from "./pages/profile/Profile";
import Register from "./pages/register/Register";
import AuthWrapper from "./store/contexts/UserContext";
import PrivateRoute from "./components/privateRoute/PrivateRoute";
import GameWrapper from './store/contexts/GameContext';
import NewWord from './pages/word/NewWord';
import Friends from './pages/friends/Friends';

export default function App() {

  return (
    <div className="appLandingPage">
      <AuthWrapper>
        <GameWrapper>
          <NavBar />
          <br />
          <Switch>
            <PrivateRoute exact path="/" component={Home} />
            <PrivateRoute exact path="/NewWord" component={NewWord} />
            <PrivateRoute exact path="/Profile" component={Profile}/>
            <PrivateRoute exact path="/Friends" component={Friends}/>
            <Route exact path="/Register" component={Register} /> 
            <Route exact path="/Login" component={Login} />
          </Switch>
          </GameWrapper>
      </AuthWrapper>
    </div>
  );
}
