/* eslint-disable react-hooks/exhaustive-deps */
import React, { useContext, useEffect, useState } from 'react';
import { GameContext } from '../../store/contexts/GameContext';
import { UserContext } from '../../store/contexts/UserContext';
import { Input } from '@material-ui/core';
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import LaunchGameButton from '../launchGameButton/LaunchGameButton';


const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1
    },
    card: {
        minWidth: 275
    },
    title: {
        fontSize: 20
    },
    pos: {
        marginBottom: 12
    },
    component: {
      padding: theme.spacing(2),
      textAlign: 'center'
    }
  })
);

const GameInterface = () => {

    const classes = useStyles();

    // Get the context of the game
    const { isTriggered, isTimeOut, randomWordsSerie, sendScore } = useContext(GameContext);
    const { username } = useContext(UserContext);

    const [randomWord, setRandomWord] = useState(''); //word the user has to type
    const [actualPoint, setActualPoint] = useState(0); //point of the word displayed
    const [userWord, setUserWord] = useState(''); //user's input
    const [score, setScore] = useState(0); //user's score
    const [precision, setPrecision] = useState(0); //user's score
    const [countSuccess, setCountSuccess] = useState(0);
    const [countFailures, setCountFailures] = useState(0);
    const [displayedText, setDisplayedText] = useState(''); // user's input displayed in input tag
    const [correctWord, setCorrectWord] = useState<string[]>([]);
    const [wrongWord, setWrongWord] = useState<string[]>([]);

    //create a tab of words and score -> connected to same index
    const [tabWords, setTabWords] = useState<string[]>([]); // collection of words the user has to type
    const [tabPoints, setTabPoints] = useState<number[]>([]);// collection of points in reference of tabWords (index)

    // Fill tabWords and tabPoints with randomWordsSerie
    const getData = () => {
        setTabPoints(tabPoints => []);
        setTabWords(tabWords => []);
        randomWordsSerie.forEach((e) => {
            const tmpWord = Object.values(e)[1];
            const tmpPoint = Object.values(e)[2];
            if(typeof tmpWord === 'string') {
                setTabWords( tabWords => [...tabWords, `${tmpWord}`]);
            };
             if(typeof tmpPoint === 'number') {
                 setTabPoints( tabPoints => [...tabPoints, tmpPoint]);
            };
        });
    }

    // Set word to type + its points
    const getRandomWord = () => {
        tabWords.shift();
        tabPoints.shift();

        //on veut le mot index 0
        if(typeof tabWords[0] === 'string') {
            setRandomWord(tabWords[0]);
        }

        setActualPoint(tabPoints[0]);
    }

    const disableOnPaste = (e: any) => {
        e.preventDefault();
    }

    const disableEnter = (e: any) => {
        if(e.keyCode === 13) {
            e.preventDefault();
        }
    }

    // Initialization of data when game started
    useEffect(() => {
        getData();
        setRandomWord(tabWords[0]);
        setActualPoint(tabPoints[0]);
    },[isTriggered, randomWordsSerie]);

    
    useEffect(() => {
        // Display user input
        setDisplayedText(userWord);

        // If user validates the answer
        if(userWord.slice(userWord.length-1) ===  ' ') {

            // Isolation of user word spaces
            let tmp = userWord.substring(0, userWord.length-1);

            // If user word = random word
            if(tmp === randomWord) { 
                setScore(score => score + actualPoint);
                setCorrectWord(correctWord => [...correctWord, randomWord]);
                setCountSuccess(success => success + 1)
                getRandomWord();
            }
            // Else we don't increment score
            else { 
                setScore(score);
                setWrongWord(wrongWord => [...wrongWord, randomWord]);
                setCountFailures(failure => failure + 1)
                getRandomWord();
            }

            setDisplayedText('');
            setUserWord('');
        }
    }, [userWord]);


    useEffect(() => {
        if(isTimeOut && (countSuccess || countFailures)) {
            const precision = countSuccess / (countSuccess + countFailures)
            setPrecision(precision)
            sendScore(score, precision, username);
            setCorrectWord(correctWord => []);
            setWrongWord(wrongWord => []);
        }
        
        if(!isTimeOut) {
            setScore(0);
            setCountFailures(0);
            setCountSuccess(0);
            setUserWord('');
        };
    }, [isTimeOut]);

    

    //si le jeu a commencé
    if(isTriggered) {
        return (
            <div className={classes.root}>
                <Paper elevation={3}>
                <Grid container spacing={3}>
                    <Grid item xs={12} md={12}>
                        <div className={classes.component}>      
                            <Typography variant='h2' gutterBottom>
                                Score : {score}
                            </Typography>
                        </div>
                    </Grid>

                    {correctWord.map((value, index) => (       
                        <div key={index}>
                            <Typography style={{color: 'green' }}> &nbsp;&nbsp;&nbsp;{value}</Typography>
                        </div>
                    ))}

                    {wrongWord.map((value, index) => (
                        <div key={index}>
                            <Typography style={{color: 'red' }}> &nbsp;&nbsp;&nbsp;{value}</Typography>
                        </div>
                    ))}

                    <Grid item xs={12} md={12}>
                        <Typography variant='h2' gutterBottom>
                            <div className={classes.component}>{randomWord}</div>
                        </Typography>
                    </Grid>

                    <Grid item xs={12} md={12}>
                        <form>
                            <Input
                            autoFocus
                            style={{width: '40%'}}
                            value={displayedText} 
                            type='text'
                            onPaste={e => disableOnPaste(e)}
                            onKeyDown={e => disableEnter(e)}
                            placeholder='Taper ici !'
                            onChange={(e: { target: { value: React.SetStateAction<string>; }; }) => setUserWord(e.target.value)} 
                        /> 
                        </form>
                    </Grid>
                </Grid>
                </Paper>
            </div>
        );
    }
    //si la partie est terminée
    if(isTimeOut) {
        return(
            <div className={classes.card}>
                <Card className={classes.root}>
                    <CardContent>
                        <Typography className={classes.title} color='textSecondary'>
                            Time is Up {username} ! 
                        </Typography>
                        <Typography variant='h5' component='h2'>
                            <br/> Score: {score} <br /> Precision: {precision.toFixed(2)}
                        </Typography>
                        <Typography className={classes.pos} color='textSecondary'>
                            Click on the Start button to start a new challenge !
                        </Typography>
                        <LaunchGameButton />
                    </CardContent>
                </Card>
            </div>
        );
    }

    //default return 
    return(
        <div className={classes.root}>
            <Paper elevation={3}>
                <Grid container spacing={3}>
                    <Grid item xs={12}>
                        <Typography variant="h5" component="h2">
                            Hello {username}, Click on the button "Start Game" when you are ready for the next challenge !
                        </Typography>
                        <Typography variant="h5" component="h2">
                            You have 1 minute to realize your best score ! (Press space to validate)
                        </Typography>
                    </Grid>
                    <Grid item xs={12} md={12}>
                        <LaunchGameButton />
                    </Grid>
                </Grid>
            </Paper>
        </div>
    );

}

export default GameInterface;
