import React, { useContext, useState } from 'react';
import Paper from '@material-ui/core/Paper';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import { UserContext } from '../../store/contexts/UserContext';
import { withRouter } from 'react-router-dom';
import { createStyles, Grid, makeStyles, Theme } from '@material-ui/core';
import logo from './Dactylo.png';


const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1
    },
    logo: {
      flexGrow: 1,
      height: 40
    }
  })
);

const NavBar = (props: any) => {

  const classes = useStyles();

  // Get the user's context
  const { isConnected, logout } = useContext(UserContext);
  const { history } = props;

  const [value, setValue] = useState(0);

  const handleButtonClick = (pageURL: string | undefined) => {
    history.push(pageURL);
  };

  const handleChange = (event: React.ChangeEvent<{}>, newValue: number) => {
    setValue(newValue);
  };

  return (
    <Paper className={classes.root}> 
      <Grid container>
        <Grid item xs={3}>
          <img src={logo} alt='Logo' height={70} className={classes.logo}/>
        </Grid>
        {isConnected ? ( 
          <Grid item xs={9}>
            <Tabs
            value={value}
            onChange={handleChange}
            indicatorColor='primary'
            textColor='primary'
            >
              <Tab label='Home' onClick={() => handleButtonClick('/')}/> 
              <Tab label='Profile' onClick={() => handleButtonClick('/Profile')}/> 
              <Tab label='New Word' onClick={() => handleButtonClick('/NewWord')}/> 
              <Tab label='Friends' onClick={() => handleButtonClick('/Friends')}/> 
              <Tab label='Logout' onClick={logout} />
            </Tabs>
          </Grid>
        )              
        : (     
          <Grid item xs={6}>
            <Tabs
            value={0}
            indicatorColor='primary'
            textColor='primary'
            centered >
              <Tab label='Login' onClick={() => handleButtonClick('/Login')}/> 
            </Tabs>
          </Grid>
        )
      }   
      </Grid>  
    </Paper>
  );
}

export default withRouter(NavBar);
