/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useContext, useState } from 'react';
import { Grid, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { GameContext } from '../../store/contexts/GameContext';

const useStyles = makeStyles({
  table: {
    maxWidth: 650
  },
  root: {
    flexGrow: 1,
  }
});


const ScoreBox = () => {

  const classes = useStyles();

  const { getScore, notifyScoreList } = useContext(GameContext);

  const [highScoreList, sethighScoreList] = useState([]);

  useEffect(() => {
    async function buildScore() {
      const responseScore = await getScore();      
      sethighScoreList(responseScore);
    }
    buildScore();
  }, [notifyScoreList])

  useEffect(() => {
    console.log('Built high-score table', highScoreList);
  }, [highScoreList])

  return (
    <div className={classes.root}>
      <Grid container spacing={3}>
        <Grid item xs={12} md={12}>
          <TableContainer component={Paper}>
            <Table className={classes.table} aria-label='simple table'>
              <TableHead>
                <TableRow>
                  <TableCell>
                    <Typography variant='h4' gutterBottom>
                      Username
                    </Typography>
                  </TableCell>
                  <TableCell>      
                    <Typography variant='h4' gutterBottom>
                      Points
                    </Typography>
                  </TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {highScoreList.map((score, index) => (
                  <TableRow key={index}>
                    <TableCell>{score['username']}</TableCell>
                    <TableCell>{score['points']}</TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        </Grid>
      </Grid>
    </div>
  );
}

export default ScoreBox;
