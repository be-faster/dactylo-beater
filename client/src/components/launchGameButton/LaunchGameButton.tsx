import { Button } from '@material-ui/core';
import React, { useContext } from 'react';
import { GameContext } from '../../store/contexts/GameContext';

const LaunchGameButton = () => {

    const { setIsTriggered,setIsTimeOut } = useContext(GameContext);

    const startGame =  () => {
        setIsTriggered(true);
        setIsTimeOut(false);
    } 
    return (
        <div>
            <Button variant='contained' color='primary' onClick={startGame}>Start Game</Button>
        </div>
    );
}

export default LaunchGameButton;
