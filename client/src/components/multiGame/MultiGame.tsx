import React, { useContext, useEffect, useState } from 'react';
import MultiTimer from '../timer/MultiTimer';
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Input from '@material-ui/core/Input'
import { UserContext } from '../../store/contexts/UserContext';
import Typography from '@material-ui/core/Typography';
import io from 'socket.io-client'
import { Button, Paper } from '@material-ui/core';

const socket = io('https://dactylobeater.website');

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1,
    },
    component: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
  }),
);

type Player = {
    username: string,
    score: number,
    inputWord: string
}

type Word = {
    word: string,
    point: number
}

const MultiGame = () => {    
    const classes = useStyles();

    const { username, isConnected } = useContext(UserContext);

    const [userWord, setUserWord] = useState(''); //user's input
    const [displayedText, setDisplayedText] = useState('');
    const [randomWord, setRandomWord] = useState<Word>({word: '', point: 0});
    const [score, setScore] = useState(0); //user's score
    const [tabUsers, setTabUsers]= useState<Player[]>([]);
    const [verifConnect, setVerifConnect] = useState(false);
    const [timer, setTimer] = useState(false);
    const [endGame, setEndGame] = useState(false);
    const [displayedBar,setDisplayedBar] = useState(false);
    const [disableBtn, setDisableBtn] = useState(false);

    useEffect(() => {
        // Display user input
        setDisplayedText(userWord);

        // If user validates the answer
        if(userWord.slice(userWord.length-1) ===  ' ') {

            // Isolation of user word spaces
            let tmp = userWord.substring(0, userWord.length-1);

            // If user word = random word
            if(tmp === randomWord.word) { 
                setScore(score => score + randomWord.point);
                getRandomWord();
            }

            // Else we don't increment score
            setDisplayedText('');
            setUserWord('');
        }
    }, [userWord, randomWord]);

   
    useEffect (()=>{
        if (isConnected){
            socket.on('player-join', (listUsers: Player[] ) => {
                setTabUsers(listUsers)
            })
            
            socket.on('player-ready', (autorisationWord: boolean) => {
                if (autorisationWord){
                    setTimer(true)
                    setTimeout(() => {
                        getRandomWord()
                    }, 5000)
                }
            })
        } 
    }, [tabUsers, isConnected])

    //UPDATE SCORE 
    useEffect( () => {
        socket.emit('update-score', score, username)
        socket.on('update-score', (listUsers: Player[], endGame: boolean) => {
                setTabUsers(listUsers)
                setEndGame(endGame)
            })
        },[score, username])

    const getRandomWord = () => {
        socket.emit('random-word',({wantWord: true}))
        socket.on('random-word', (randomWordSocket: Word)=>{           
            setRandomWord({word: randomWordSocket.word, point: randomWordSocket.point}) 
        })  
    }

    const disableOnPaste = (e: any) => {
        e.preventDefault();
    }

    const disableEnter = (e: any) => {
        if(e.keyCode === 13) {
            e.preventDefault();
        }
    }
 
    const connectSocket = () => {
        if(!verifConnect){
            setVerifConnect(true)
            socket.emit('player-join', ({username}))
        }
    }

    const readySocket= ()=> {
        setDisplayedBar(true)
        setDisableBtn(true)
        socket.emit('player-ready', {username})
    }

    const leaveRoom = () => {
        setVerifConnect(false)
        setEndGame(false)
        setDisplayedBar(false)
        setTimer(false)
        setRandomWord({word: '', point: 0})
        setDisableBtn(false)
    }

    const displayRandomWord = () => {
        if(verifConnect && !endGame) {
            return(
                <Grid item xs={12}>
                    <Typography variant='h2' gutterBottom>
                        <div className={classes.component}>{randomWord.word}</div>
                    </Typography>
                </Grid> 
            )
        } else if (endGame){
            socket.emit('manual-disconnect', () => {
                socket.disconnect()
                setTabUsers([])
            })
            if(score >= 100){
                return(
                    <div>
                        <h1>You WIN !</h1>
                        <Button variant='contained' color='primary' onClick={leaveRoom}>Leave Room </Button>
                    </div>
                )
            } else {
                return(
                    <div>
                        <h1>You LOOSE...</h1>
                        <Button variant='contained' color='primary' onClick={leaveRoom}>Leave Room </Button>
                    </div>
                )
            }
        }
    }

    const displayTimer = () => {
        if(timer) {
            return(
                <Grid item xs={12} md={12}>
                    <div className={classes.component}>
                        <MultiTimer isReady={timer}/>
                    </div>
                </Grid>
            )
        }
    }

    const renderPlayers = () => {
        if(verifConnect && !endGame) {
            return(
                tabUsers.map((e, index) => (
                    <div key = {index}>
                        <h1 >
                            <span>{e.username} : </span>
                            <span>{e.score}</span>
                        </h1>
                    </div>
                ))
            )
        }
    }
    
    const buttonConnectReady = ()=> {
        if (verifConnect) {
            return (
                <Button variant='contained' color='primary' disabled={disableBtn} onClick={readySocket}>Ready </Button>
            )
        } else {
            return(
                <div>
                    <Button variant='contained' color='primary' onClick={connectSocket}>Join Room</Button>
                    <Paper elevation={3}>
                        <Typography variant="h5" component="h2">
                            The first at 100 points will WIN ! (Press space to validate)
                        </Typography>
                    </Paper>
                </div>
            )
        }
    }

    return (
        <div className={classes.root}>
            <Grid container spacing={3}>
                <Grid item xs={12} md={12}>
                        <div className={classes.component}>
                            {verifConnect && displayTimer()}
                            <div>
                                {buttonConnectReady()}
                            </div>
                            {verifConnect && displayedBar && !endGame &&
                                <Paper elevation={3}>
                                    {displayRandomWord()}
                                    <form>
                                        <Input
                                            style={{width: '40%'}}
                                            value={displayedText} 
                                            type='text'
                                            onPaste={e => disableOnPaste(e)}
                                            onKeyDown={e => disableEnter(e)}
                                            placeholder='Taper ici !'
                                            onChange={(e: { target: { value: React.SetStateAction<string>; }; }) => setUserWord(e.target.value)} 
                                        /> 
                                    </form>
                                </Paper>
                            }
                            {endGame && verifConnect &&
                                <Paper elevation={3}>
                                    {displayRandomWord()}
                                </Paper>
                            }
                        </div>
                    </Grid>
                <Grid item xs={9} md={9}></Grid>
                <Grid item xs={3} md={3}>
                    <Paper elevation={3}>
                        {renderPlayers()}
                    </Paper>
                </Grid>
            </Grid>
        </div>
    );
}

export default MultiGame;
