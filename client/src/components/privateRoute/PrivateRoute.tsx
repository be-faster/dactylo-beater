import React, { useContext } from 'react';
import { UserContext } from '../../store/contexts/UserContext';
import { Route, Redirect } from 'react-router-dom';

type Props = {
    component: React.FC,
    path: string,
    exact: boolean
}

const PrivateRoute: React.FC<Props> = ({ component, path, exact }) => {

    const { isConnected } = useContext(UserContext);

    return isConnected ? (<Route exact={exact} path={path} component={component} />) : (<Redirect to='/Login'/>);

}

export default PrivateRoute;
