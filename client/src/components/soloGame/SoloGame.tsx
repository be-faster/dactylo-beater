import React from 'react';
import Timer from '../timer/Timer';
import GameInterface from '../gameInterface/GameInterface';
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import ScoreBox from '../highScore/ScoreBox';
import { GameContext } from '../../store/contexts/GameContext';
import { useContext } from 'react';


const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1
    },
    component: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary
    }
  }),
);


const SoloGame = () => {
    const {fullWidth} = useContext(GameContext);
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <Grid container spacing={3}>
                <Grid item xs={fullWidth? 12: 9} md={fullWidth? 12 : 9}>
                    <Grid item xs={12} md={12}>
                        <div className={classes.component}>
                            <Timer/>
                        </div>
                    </Grid>
                    <Grid item xs={12} md={12}>
                        <div className={classes.component}>
                            <GameInterface /> 
                        </div>
                    </Grid>
                </Grid>
                <Grid item xs={fullWidth ? 1 : 3 } md={fullWidth ? 1 : 3 }>
                    {!fullWidth && <ScoreBox />}
                </Grid>
            </Grid>
        </div>
    );
}

export default SoloGame;
