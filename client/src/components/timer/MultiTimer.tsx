/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from 'react';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import { Grid } from '@material-ui/core';

const useStyles = makeStyles({
    root: {
      textAlign: 'center',
    },
  });

  type Props = {
      isReady: boolean
  };

const MultiTimer: React.FC<Props> = ({ isReady }) => {

    const classes = useStyles();

    const [seconds, setSeconds] = useState(5);

    useEffect(() => {
        let interval : any = null;
        if(isReady) {
            interval = setInterval(() => {
                setSeconds(seconds => seconds - 1);
            }, 1000);
        }
        if(seconds === 0) {
            clearInterval(interval);
            setSeconds(0);
        }

        if(!isReady) clearInterval(interval);
        
        // allows the timer to not break
        return () => clearInterval(interval);
    }, [isReady, seconds]); // call useEffect() when one of the values are modifieds
    
    if (seconds !== 0) { 
        return(
        <div className={classes.root}>
            <Paper elevation={3}>
                <Typography variant='h1'>
                    {seconds}s
                </Typography>
                <Grid container justify='flex-end' alignItems='flex-end'>
                </Grid>
            </Paper>
        </div>
        );
    }

    return (
        <div className={classes.root}>
            <Paper elevation={3}>
                <Typography variant='h1'>
                    GO !
                </Typography>
                <Grid container justify='flex-end' alignItems='flex-end'>
                </Grid>
            </Paper>
        </div>
    );
}


export default MultiTimer;