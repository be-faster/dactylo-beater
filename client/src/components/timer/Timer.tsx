/* eslint-disable react-hooks/exhaustive-deps */
import React, { useContext, useEffect, useState } from 'react';
import {GameContext} from '../../store/contexts/GameContext';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import { Grid, IconButton } from '@material-ui/core';
import FullscreenIcon from '@material-ui/icons/Fullscreen';
import FullscreenExitIcon from '@material-ui/icons/FullscreenExit';

const useStyles = makeStyles({
    root: {
      textAlign: 'center'
    }
});

const Timer = () => {

    const classes = useStyles();

    const { isTriggered, setIsTriggered, setIsTimeOut, setFullWidth, fullWidth } = useContext(GameContext);

    const [seconds, setSeconds] = useState(60);

    useEffect(() => {
        let interval : any = null;
        if(isTriggered) {
            interval = setInterval(() => {
                setSeconds(seconds => seconds - 1);
            }, 1000);
        }

        if(seconds === 0) {
            setIsTriggered(false);
            setIsTimeOut(true);
            setSeconds(60);
        }

        if(!isTriggered) clearInterval(interval);
        
        // allows the timer to not break
        return () => clearInterval(interval);
    }, [isTriggered, seconds, setIsTriggered]); // call useEffect() when one of the values are modifieds

    return(
        <div className={classes.root}>
            <Paper elevation={3}>
                <Typography variant='h1'>
                    {seconds}s
                </Typography>
                <Grid container justify='flex-end' alignItems='flex-end'>
                    <IconButton style={{ bottom: 3, right: 3 }} aria-label='fullscreen' onClick={() => { setFullWidth(!fullWidth) }}>
                        {fullWidth ? <FullscreenExitIcon/> :
                            <FullscreenIcon />
                        }
                    </IconButton>
                </Grid>
            </Paper>
        </div>
    );
}

export default Timer;
