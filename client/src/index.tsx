import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { BrowserRouter as Router } from 'react-router-dom';
import { createMuiTheme } from '@material-ui/core';
import CssBaseline from '@material-ui/core/CssBaseline';
import { ThemeProvider } from '@material-ui/styles';
import lightBlue from '@material-ui/core/colors/lightBlue';
import deepOrange from '@material-ui/core/colors/deepOrange';
import monochrome from './assets/monochrome.jpg';


const theme = createMuiTheme({
  palette: {
    primary: lightBlue,
    secondary: deepOrange
  },
  overrides: {
    MuiCssBaseline: {
      '@global': {
        body: {
          backgroundImage: `url(${monochrome})`,
          backgroundSize: '100%'
        }
      }
    }
  },
  typography: {
    h3: {
      color: "#FFFAFA",
    }
  }
});

const rootElement = document.getElementById('root');
ReactDOM.render(
  <React.StrictMode>
    <Router>
      <ThemeProvider theme={theme}>
      <CssBaseline />
       <App />
      </ThemeProvider>
    </Router>
  </React.StrictMode>,
  rootElement
);
