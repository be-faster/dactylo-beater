/* eslint-disable react-hooks/exhaustive-deps */
import { Button, createStyles, Radio, Grid, Input, makeStyles, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Theme } from '@material-ui/core';
import React, { useContext, useEffect, useState } from 'react';
import { UserContext } from '../../store/contexts/UserContext';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/styles';
import Paper from '@material-ui/core/Paper';
import background from '../../assets/background-friends.jpg';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1
    },
    grid: {
        padding: theme.spacing(3),
        textAlign: 'center',
        align: 'center',
        justify:'center'
    }, 
    components: {
        margin: '10px'
    },
    head: {
        backgroundColor: theme.palette.common.black,
        color: theme.palette.common.white
    },
    body: {
    fontSize: 14
    },
    table: {
        minWidth: 700
    }
  })
);

const StyledTableRow = withStyles((theme: Theme) =>
  createStyles({
    root: {
      '&:nth-of-type(odd)': {
        backgroundColor: theme.palette.action.hover
      }
    }
  }),
)(TableRow);

const StyledTableCell = withStyles((theme: Theme) =>
  createStyles({
    head: {
      backgroundColor: theme.palette.common.black,
      color: theme.palette.common.white
    },
    body: {
      fontSize: 14
    }
  })
)(TableCell);

const Friends = () => {

    // Set style of the component
    const classes = useStyles();

    // Get contexts
    const { isConnected, addNewFriend, getUserFriendHighScore, getUserFriendHighAccuracy } = useContext(UserContext);

    // Set states of the component
    const [friend, setFriend] = useState('');
    const [friendsListPoint, setFriendsListPoint] = useState([]);
    const [friendsListAccuracy, setFriendsListAccuracy] = useState([]);
    const [selectedValue, setSelectedValue] = useState('points');
    const [submitResponse, setSubmitResponse] = useState('');
    
    // Get the friends list
    useEffect(() => {
        async function buildFriendsList() {
          const responseFriendsA = await getUserFriendHighScore(); 
          if (responseFriendsA !== null) {
              console.log('Set FriendsList High Score Point', responseFriendsA.friendsHighScore);
              setFriendsListPoint(responseFriendsA.friendsHighScore);
          }

          const responseFriendsB = await getUserFriendHighAccuracy(); 
          if (responseFriendsB !== null) {
            console.log('Set FriendsList High Score Accuracy', responseFriendsB.friendsHighAccuracy);
            setFriendsListAccuracy(responseFriendsB.friendsHighAccuracy);
          }
        }
        buildFriendsList();
    }, [submitResponse])

    useEffect(() => {
        console.log('Connection user : ', isConnected);
    });

    const submit = async () => {
      try {
        const response = await addNewFriend(friend);

        // Reset after submition new word
        setFriend('');

        // Response to the user
        if(response !== null) {
          setSubmitResponse('');
          setSubmitResponse('Friend added !');
        } else {
          setSubmitResponse('');
          setSubmitResponse('Enter a correct friend please !');
        }
      } catch (error) {
        console.log(error);
      }
    }

    const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
      setSelectedValue(event.target.value);      
    }

    return (
      <div className={classes.root}>
          <Grid container spacing={3}>
              <Grid className={classes.grid} item xs={2} md={2}></Grid>
              <Grid className={classes.grid} item xs={8} md={8}> 
                <Card>
                    <CardActionArea disabled>
                        <CardMedia
                        component='img'
                        alt='background-friends_img'
                        height='200'
                        src={background}
                        title='background-friends_img'
                        />
                    </CardActionArea>
                    <CardContent>
                      <Typography gutterBottom variant='h2' component='h2'>
                          Add your friends here !
                      </Typography>
                      <Input
                          autoFocus
                          style={{width: '30%'}}
                          value={friend} 
                          type='text'
                          placeholder='Add your friend'
                          onChange={(e) => setFriend(e.target.value) } 
                      /> 
                      <Button className={classes.components} variant='contained' color='primary' onClick={submit}>
                        Add
                      </Button>
                      {submitResponse}
                    </CardContent>
                    Points
                    <Radio
                      checked={selectedValue === 'points'}
                      onChange={handleChange}
                      value='points'
                      name='radio-button-points'
            
                    />
                    Accuracy
                    <Radio
                      checked={selectedValue === 'accuracy'}
                      onChange={handleChange}
                      value='accuracy'
                      name='radio-button-accuracy'

                    />
                    <CardActions>
                      <TableContainer component={Paper}>
                        <Table className={classes.table} aria-label='customized table'>
                          <TableHead>
                            <TableRow>
                                <StyledTableCell>ID</StyledTableCell>
                                <StyledTableCell>Username</StyledTableCell>
                                <StyledTableCell>High Score Point</StyledTableCell>
                                <StyledTableCell>Accuracy</StyledTableCell>
                                <StyledTableCell>Date</StyledTableCell>
                            </TableRow>
                            </TableHead>
                            <TableBody>
                            { selectedValue === 'points' &&
                            friendsListPoint.map((row, index) => (
                                <StyledTableRow key={index}>
                                    <StyledTableCell>{row['ID_User']}</StyledTableCell>
                                    <StyledTableCell>{row['username']}</StyledTableCell>
                                    <StyledTableCell>{'highPoint' in row ? row['highPoint'] : '-'}</StyledTableCell>
                                    <StyledTableCell>{'accuracy' in row ? row['accuracy'] : '-'}</StyledTableCell>
                                    <StyledTableCell>{isNaN(new Date(row['dateScore']).getTime()) ? '-' : new Date(row['dateScore']).toLocaleString() }</StyledTableCell>
                                </StyledTableRow>
                            )) }
                            { selectedValue === 'accuracy' && 
                            friendsListAccuracy.map((row, index) => (
                                <StyledTableRow key={index}>
                                    <StyledTableCell>{row['ID_User']}</StyledTableCell>
                                    <StyledTableCell>{row['username']}</StyledTableCell>
                                    <StyledTableCell>{'points' in row ? row['points'] : '-'}</StyledTableCell>
                                    <StyledTableCell>{'highAccuracy' in row ? row['highAccuracy'] : '-'}</StyledTableCell>
                                    <StyledTableCell>{isNaN(new Date(row['dateScore']).getTime()) ? '-' : new Date(row['dateScore']).toLocaleString() }</StyledTableCell>
                                </StyledTableRow>
                            ))}
                            </TableBody>
                        </Table>
                      </TableContainer>
                    </CardActions>
                  </Card>
                </Grid>
              <Grid className={classes.grid} item xs={2} md={2}></Grid>
          </Grid>
      </div>
    );
}

export default Friends;
