/* eslint-disable react-hooks/exhaustive-deps */
import React, { useContext, useEffect, useState } from 'react'
import SoloGame from '../../components/soloGame/SoloGame';
import {UserContext} from '../../store/contexts/UserContext';
import MultiGame from '../../components/multiGame/MultiGame';
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import { GameContext } from '../../store/contexts/GameContext';
import { Button, Typography } from '@material-ui/core';


const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
        flexGrow: 1
    },
    component: {
        textAlign: 'center',
        color: theme.palette.text.secondary,
        padding: theme.spacing(2)
    },
  }),
);

const Home = () => {
    const classes = useStyles();
    const { isConnected } = useContext(UserContext);
    const { fullWidth } = useContext(GameContext);
    const [gameType, gameTypeContext] = useState(true);
    var [gameName, gameNameContext] = useState('');

    useEffect (()=> {
        if (!gameType){
             gameNameContext('ONE PLAYER');
        }else {
            gameNameContext('MULTI PLAYER');
        }
    }, [gameType])

    useEffect(() => {
    }, []);

    useEffect(() => {
    }, [fullWidth]);

    const selectGame= function  (){
        if (gameType) {
            return <SoloGame />
        }else {
            return <MultiGame/>
        }   
    }

    if (isConnected) {
        return ( 
            <div className={classes.root}>
                <Grid container className={classes.component}>
                    <Grid item xs={12} md={12}>
                        <Typography variant="h3" gutterBottom>
                            Choose your mode:
                        </Typography>
                    </Grid>
                    <Grid item xs={12} md={12}>
                        <Button variant='contained' color='primary' onClick ={()=> gameTypeContext(true)}>Solo Game</Button>
                        &nbsp;
                        <Button variant='contained' color='primary' onClick ={()=> gameTypeContext(false)}>Multi Game</Button>
                    </Grid>
                    <Grid item xs={12} md={12}>
                         {selectGame()}
                    </Grid>
                </Grid>
            </div>
        );
    } else {
        return (
            <div>
                Veuillez vous connecter.
            </div>
        );
    }
}

export default Home;
