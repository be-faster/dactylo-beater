import React, { useContext, useEffect, useState } from 'react';
import { Button, TextField, Grid, Typography, Link, Container, CssBaseline, Avatar } from '@material-ui/core';
import {  makeStyles } from '@material-ui/core/styles';
import { UserContext } from '../../store/contexts/UserContext';
import { withRouter } from 'react-router-dom';
import './Login.css';

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center'
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1)
  },
  submit: {
    margin: theme.spacing(3, 0, 2)
  }
}));

const Login = (props: any) => {
  const classes = useStyles();

  const { history } = props;
  const { login, setUsername, setPassword, isConnected, errorConnection } = useContext(UserContext);
  
  const[showErrorconn, setShowError] = useState(false);
  
  useEffect(() => {
    if(isConnected){
      history.push('/');
    }
  });

  const LoginCTA = () => {
    login();
    if(isConnected) {
      setShowError(false);
    } else {
      setShowError(true);
    }
  }

  return (
    <Container component='main' maxWidth='xs' className='formContainer'>
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className='test' />
        <Typography component='h1' variant='h5'>
          Sign in
        </Typography>
        <form className={classes.form} noValidate>
          <TextField
            variant='outlined'
            margin='normal'
            required
            fullWidth
            id='username'
            label='User Name'
            name='username'
            autoComplete='username'
            autoFocus
            onChange={e => { setUsername(e.target.value) }}
          />
          <TextField
            variant='outlined'
            margin='normal'
            required
            fullWidth
            name='password'
            label='Password'
            type='password'
            id='password'
            autoComplete='current-password'
            onChange={e => { setPassword(e.target.value) }}
          />
          <Button
            fullWidth
            variant='contained'
            color='primary'
            className={classes.submit}
            onClick={LoginCTA}
          >
            Sign In
          </Button>
          {showErrorconn &&
            <p className='errorMessage'>{errorConnection}</p>
          } 
          <Grid container>
            <Grid item xs>
            </Grid>
            <Grid item>
              <Link href='/Register' variant='body2'>
                {'Don\'t have an account? Sign Up'}
              </Link>
            </Grid>
          </Grid>
        </form>
      </div>
    </Container>
  );
}

export default withRouter(Login);
