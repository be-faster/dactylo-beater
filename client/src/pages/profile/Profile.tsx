import { createStyles, Grid, makeStyles, Theme } from '@material-ui/core';
import React, { useContext, useEffect } from 'react'
import { UserContext } from '../../store/contexts/UserContext';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import Statistics from './Statistics';
import UserScoreTable from './UserScoreTable';
import UserChartPoint from './UserChartPoint';
import UserChartAccuracy from './UserChartAccuracy';
import background from '../../assets/background-profile.jpg';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1
    },
    component: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
        align: 'center',
        justify:'center'
    }, 
    container: {
      maxHeight: 440
    }
  })
);

const Profile = () => {

    const classes = useStyles();

    const { username, isConnected } = useContext(UserContext);

    useEffect(() => {
        console.log('Connection user : ', isConnected);
    });

    return (
        <div className={classes.root}>
            <Grid container spacing={3}>
                <Grid className={classes.component} item xs={2}></Grid>
                <Grid className={classes.component} item xs={8}> 
                    <Card>
                        <CardActionArea disabled>
                            <CardMedia
                            component='img'
                            alt='background-profile_img'
                            height='200'
                            src={background}
                            title='background-profile_img'
                            />
                            <CardContent>
                                <Typography gutterBottom variant='h1' component='h2'>
                                    {username}
                                </Typography>
                                <Typography variant='h4' color='textSecondary' component='p'>
                                    Statistics of {username} 
                                </Typography>
                            </CardContent>
                        </CardActionArea>
                    </Card>
                    {/* Components used in profile page */}
                    <Statistics />
                    <UserScoreTable />
                    <UserChartPoint />
                    <UserChartAccuracy />
                </Grid>
                <Grid className={classes.component} item xs={2}></Grid>
            </Grid>
        </div>
    );
}

export default Profile;
