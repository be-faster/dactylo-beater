/* eslint-disable react-hooks/exhaustive-deps */
import React, { useContext, useEffect, useState } from 'react';
import Paper from '@material-ui/core/Paper';
import { UserContext } from '../../store/contexts/UserContext';
import { Grid, Typography } from '@material-ui/core';
import EqualizerIcon from '@material-ui/icons/Equalizer';
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward';
import ArrowUpwardIcon from '@material-ui/icons/ArrowUpward';
import GpsFixedIcon from '@material-ui/icons/GpsFixed';
import Tooltip from '@material-ui/core/Tooltip';

const UserChartPoint = () => {

  const { getUserScore } = useContext(UserContext);

  const [userScoreList, setUserScore] = useState([]);
  const [userMeanPoint, setUserMeanPoint] = useState(0);
  const [userMeanAccuracy, setUserMeanAccuracy] = useState(0);
  const [userMax, setUserMax] = useState(0);
  const [userMin, setUserMin] = useState(0);

  useEffect(() => {
    async function buildUserScore() {
      const responseUserScore = await getUserScore();  
      setUserScore(responseUserScore);
    }
    buildUserScore();
  }, []);

  useEffect(() => {
    console.log('Built UserScoreList (CHART)', userScoreList)

    let totalMeanPoint: number = 0;
    let totalMeanAccuracy: number = 0;
    for(let score of userScoreList) {
        totalMeanPoint += score['points'];
        totalMeanAccuracy += score['accuracy'];
    }
    const meanPoint = totalMeanPoint / userScoreList.length;
    setUserMeanPoint(meanPoint);

    const meanAccuracy = totalMeanAccuracy / userScoreList.length;
    setUserMeanAccuracy(meanAccuracy);

    setUserMax(Math.max.apply(Math, userScoreList.map((o) => { return o['points']; })) )
    setUserMin(Math.min.apply(Math, userScoreList.map((o) => { return o['points']; })) )
  }, [userScoreList])
 
  return (
    <Paper>
        <Grid container spacing={3}>
            <Grid item xs={6}> 
                <Tooltip title='Average Point' arrow>
                    <Typography variant='h2' gutterBottom>
                        <EqualizerIcon fontSize='large'/>
                        {userMeanPoint.toFixed(2)}
                    </Typography>
                </Tooltip>
            </Grid>
            <Grid item xs={6}> 
                <Tooltip title='Average Accuracy' arrow>
                    <Typography variant='h2' gutterBottom>
                        <GpsFixedIcon fontSize='large'/>
                        {userMeanAccuracy.toFixed(2)}
                    </Typography>
                </Tooltip>
            </Grid>
            <Grid item xs={6}>
                <Tooltip title='Maximum Score' arrow>
                    <Typography variant='h2' gutterBottom>
                        <ArrowUpwardIcon fontSize='large'/>
                        {userMax.toFixed(2)}
                    </Typography>
                </Tooltip>
            </Grid>  
            <Grid item xs={6}>
                <Tooltip title='Minimum Score' arrow>
                    <Typography variant='h2' gutterBottom>
                        <ArrowDownwardIcon fontSize='large'/>
                        {userMin.toFixed(2)}
                    </Typography>
                </Tooltip>
            </Grid>
        </Grid>
    </Paper>
  )
};

export default UserChartPoint;
