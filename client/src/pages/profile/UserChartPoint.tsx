/* eslint-disable react-hooks/exhaustive-deps */
import React, { useContext, useEffect, useState } from 'react';
import Paper from '@material-ui/core/Paper';
import { ArgumentAxis, ValueAxis, Chart, LineSeries, Title } from '@devexpress/dx-react-chart-material-ui';
import { UserContext } from '../../store/contexts/UserContext';
import { TablePagination } from '@material-ui/core';

const UserChartPoint = () => {

  const { getUserScore } = useContext(UserContext);

  const [userScoreList, setUserScore] = useState([]);
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const [page, setPage] = useState(0);

  useEffect(() => {
    async function buildUserScore() {
      const responseUserScore = await getUserScore();  
      responseUserScore.sort((a: any, b: any) => new Date(a['dateScore']).getTime() - new Date(b['dateScore']).getTime());
      responseUserScore.map((score: any ) => score['dateScore'] = new Date(score['dateScore']).toLocaleString().replace('à', '\n'));
      setUserScore(responseUserScore);
    }
    buildUserScore()
  }, []);

  useEffect(() => {
    console.log('Built UserScoreList (CHART)', userScoreList);
    if(userScoreList.length) {
      setPage(userScoreList.length / rowsPerPage - 1);
    }
  }, [userScoreList])
  
  const handleChangePage = (event: unknown, newPage: number) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement>) => {
    setRowsPerPage(+event.target.value);
    setPage(userScoreList.length / +event.target.value - 1);
  };
  return (
    <Paper>
      <Chart data={userScoreList.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)}>
        <Title text='Point' />
        <ArgumentAxis />
        <ValueAxis />
        <LineSeries valueField='points' argumentField='dateScore'/>
      </Chart>
      <TablePagination
          rowsPerPageOptions={[5, 7]}
          component='div'
          count={userScoreList.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        />
    </Paper>
  )
};

export default UserChartPoint;
