/* eslint-disable react-hooks/exhaustive-deps */
import { createStyles, makeStyles, Radio, Theme, Typography, withStyles } from '@material-ui/core';
import React, { useContext, useEffect, useState } from 'react'
import { UserContext } from '../../store/contexts/UserContext';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import TablePagination from '@material-ui/core/TablePagination';


const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1
    }, 
    container: {
      maxHeight: 440
    }
  })
);

const StyledTableCell = withStyles((theme: Theme) =>
  createStyles({
    head: {
      backgroundColor: theme.palette.common.black,
      color: theme.palette.common.white
    },
    body: {
      fontSize: 14
    }
  })
)(TableCell);


const UserScoreTable = () => {
  const classes = useStyles();

  const { getUserScore } = useContext(UserContext);
    
  const [userScoreList, setUserScore] = useState([]);
  const [selectedValue, setSelectedValue] = useState('points');
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(5);

  useEffect(() => {
      async function buildUserScore() {
        const responseUserScore = await getUserScore();
        console.log(responseUserScore);
        responseUserScore.sort((a: any, b: any) => b['points'] - a['points']);
        setUserScore(responseUserScore);
      }
      buildUserScore()
  }, []);

  useEffect(() => {
    console.log('Built UserScoreList (TABLE) ', userScoreList);
  }, [userScoreList])

  useEffect(() => {
    if(selectedValue === 'points') {
      const sorted = [...userScoreList].sort((a, b) => b['points'] - a['points']);
      setUserScore(sorted);
    } else if (selectedValue === 'accuracy') {
      const sorted = [...userScoreList].sort((a, b) => b['accuracy'] - a['accuracy']);
      setUserScore(sorted);

    } else if (selectedValue === 'date') {
      const sorted = [...userScoreList].sort((a: any, b: any) => new Date(b['dateScore']).getTime() - new Date(a['dateScore']).getTime());
      setUserScore(sorted);
    } 
  }, [selectedValue])

  const handleChangePage = (event: unknown, newPage: number) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement>) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setSelectedValue(event.target.value);      
  };

  return (           
    <Paper className={classes.root}>
      <Typography variant='h4' color='textSecondary' component='p'>
        Your passed games
      </Typography>
      Points
      <Radio
        checked={selectedValue === 'points'}
        onChange={handleChange}
        value='points'
        name='radio-button-points'
      />
      Accuracy
      <Radio
        checked={selectedValue === 'accuracy'}
        onChange={handleChange}
        value='accuracy'
        name='radio-button-accuracy'
      />
      Date
      <Radio
        checked={selectedValue === 'date'}
        onChange={handleChange}
        value='date'
        name='radio-button-date'
      />
      <TableContainer className={classes.container}>
        <Table stickyHeader aria-label='sticky table'>
          <TableHead>
            <TableRow>
              <StyledTableCell align='center'>Points</StyledTableCell>
              <StyledTableCell align='center'>Accuracy</StyledTableCell>
              <StyledTableCell align='center'>Date</StyledTableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {userScoreList.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((row, index) => {
            return (
              <TableRow hover role='checkbox' tabIndex={-1} key={index}>
                <StyledTableCell align='center'>{row['points']}</StyledTableCell>
                <StyledTableCell align='center'>{row['accuracy']}</StyledTableCell>
                <StyledTableCell align='center'>{new Date(row['dateScore']).toLocaleString()}</StyledTableCell>
              </TableRow>
              );
            })}
        </TableBody>
      </Table>
    </TableContainer>
    <TablePagination
      rowsPerPageOptions={[5, 10, 20]}
      component='div'
      count={userScoreList.length}
      rowsPerPage={rowsPerPage}
      page={page}
      onChangePage={handleChangePage}
      onChangeRowsPerPage={handleChangeRowsPerPage}
    />
    </Paper>
  );
}

export default UserScoreTable;
