import React, { useContext, useEffect, useState } from 'react';
import { Button, TextField, Typography, Container, CssBaseline, Avatar } from '@material-ui/core';
import {  makeStyles } from '@material-ui/core/styles';
import {withRouter} from 'react-router-dom';
import {UserContext} from '../../store/contexts/UserContext';
import './Register.css';

const useStyles = makeStyles((theme) => ({
    paper: {
      marginTop: theme.spacing(8),
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center'
    },
    avatar: {
      margin: theme.spacing(1),
      backgroundColor: theme.palette.secondary.main,
    },
    form: {
      width: '100%', // Fix IE 11 issue.
      marginTop: theme.spacing(1),
    },
    submit: {
      margin: theme.spacing(3, 0, 2),
    },
}));


const Register = (props: any) => {

  const classes = useStyles();

  const { history } = props;
  const { setUsername, setPassword, setEmail, register } = useContext(UserContext);

  const [initialPwd, setInitialPwd ] = useState('');
  const [confirmPwd, setconfirmPwd] = useState('');
  const [matchPwd, setmatchPwd] = useState(true);
  const [initialEmail, setInitialEmail] = useState('');
  const [initialUsername, setInitialUsername] = useState('');

  useEffect(() => {
    if(initialPwd.length && confirmPwd && initialPwd === confirmPwd) {
      setPassword(initialPwd);
      setEmail(initialEmail);
      setUsername(initialUsername);
    }
  })

  const checkPwd = () => {
      if(initialPwd.length && confirmPwd.length && initialPwd !== confirmPwd) {
        setmatchPwd(false);
      }
      if(initialPwd.length && confirmPwd && initialPwd === confirmPwd) {
        setmatchPwd(true);
        register();
        history.push('/Login'); 
      }
  }

return (
    <Container component='main' maxWidth='xs' className='formContainer'>
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
        </Avatar>
        <Typography component='h1' variant='h5'>
          Sign Up
        </Typography>
        <form className={classes.form} noValidate>
        <TextField
            variant='outlined'
            margin='normal'
            required
            fullWidth
            id='email'
            label='Email Address'
            name='email'
            autoComplete='email'
            autoFocus
            onChange={(e) => {setInitialEmail(e.target.value)}}
          />
          <TextField
            variant='outlined'
            margin='normal'
            required
            fullWidth
            id='username'
            label='User Name'
            name='username'
            autoComplete='username'
            autoFocus
            onChange={(e) => {setInitialUsername(e.target.value)}}
          />
          <TextField
            variant='outlined'
            margin='normal'
            required
            fullWidth
            name='password'
            label='Password'
            type='password'
            id='password'
            autoComplete='current-password'
            onChange={(e) => { setInitialPwd(e.target.value); }}
          />
          <TextField
          variant='outlined'
          margin='normal'
          required
          fullWidth
          name='passwordConfirm'
          label='Confirm Password'
          type='password'
          id='passwordConfirm'
          autoComplete='current-password'
          onChange={(e) => { setconfirmPwd(e.target.value) }}
          />
          {!matchPwd &&
              <p className='errorMessage'>Mot de Passe non confirmé</p>
          }
          <Button
            fullWidth
            variant='contained'
            color='primary'
            className={classes.submit}
            onClick={checkPwd}
          >
            Sign Up
          </Button>
        </form>
      </div>
    </Container>
  );
}
export default withRouter(Register);
