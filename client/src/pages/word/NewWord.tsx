import { Button, createStyles, FormHelperText, Grid, Input, InputLabel, makeStyles, MenuItem, Select, Theme } from '@material-ui/core';
import React, { useContext, useEffect, useState } from 'react';
import { UserContext } from '../../store/contexts/UserContext';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import { GameContext } from '../../store/contexts/GameContext';
import background from '../../assets/background-words.jpg';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1,
    },
    grid: {
        padding: theme.spacing(3),
        textAlign: 'center',
        align: 'center',
        justify:'center'
    }, 
    components: {
        margin: '10px'
    }
  }),
);

const NewWord = () => {

    // Set style of the component
    const classes = useStyles();
    
    // Get contexts
    const { isConnected } = useContext(UserContext);
    const { sendNewWord } = useContext(GameContext);

    // Set states of the component
    const [newWord, setNewWord] = useState('');
    const [submitResponse, setSubmitResponse] = useState('');
    const [point, setPoint] = React.useState<string | number>(1);
    const [open, setOpen] = React.useState(false);
    
    useEffect(() => {
        console.log('Connection user : ', isConnected)
    });
  
    const handleChange = (event: React.ChangeEvent<{ value: unknown }>) => {
      setPoint(event.target.value as number);
    };
  
    const handleClose = () => {
      setOpen(false);
    };
  
    const handleOpen = () => {
      setOpen(true);
    };

    const submit = async () => {
      try {
        const response = await sendNewWord(newWord, +point); 
        // Reset after submition new word
        setNewWord(''); 
        setPoint(1);

        // Response to the user
        if(response !== null) {
          setSubmitResponse('Word submitted !')
        } else {
          setSubmitResponse('Word already exists !')
        }
      } catch (error) {
        console.log(error)
      }
    }
    return (
      <div className={classes.root}>
          <Grid container spacing={3}>
              <Grid className={classes.grid} item xs={2}></Grid>
              <Grid className={classes.grid} item xs={8}> 
                <Card>
                  <CardActionArea disabled>
                      <CardMedia
                      component='img'
                      alt='background-words_img'
                      height='200'
                      src={background}
                      title='background-words_img'
                      />
                  </CardActionArea>
                  <Typography gutterBottom variant='h2' component='h2'>
                      Add some words for more fun !
                  </Typography>
                  <Typography gutterBottom variant='h4' color='textSecondary' component='h2'>
                      Contribute to the game by adding more words 
                  </Typography>
                  <Grid className={classes.grid} item xs={12}> 
                    <form>
                      <Input
                          autoFocus
                          style={{width: '30%'}}
                          value={newWord} 
                          type='text'
                          placeholder='Your new word'
                          onChange={(e) => setNewWord(e.target.value) } 
                      /> 
                      <Select
                        className={classes.components}
                        labelId='selectPoint'
                        style={{width: '30%'}}
                        open={open}
                        onClose={handleClose}
                        onOpen={handleOpen}
                        value={point}
                        onChange={handleChange}
                        defaultValue='1'
                        >
                        <MenuItem value={1}>1 point</MenuItem>
                        <MenuItem value={2}>2 points</MenuItem>
                        <MenuItem value={3}>3 points</MenuItem>
                        <MenuItem value={4}>4 points</MenuItem>
                        <MenuItem value={5}>5 points</MenuItem>
                      </Select>
                      <Button className={classes.components} variant='contained' color='primary' onClick={submit}>
                        Send
                      </Button>
                    </form>
                  </Grid>
                {submitResponse}
              </Card>
            </Grid>
          <Grid className={classes.grid} item xs={2}></Grid>
        </Grid>
      </div>
    );
    
}


export default NewWord;
