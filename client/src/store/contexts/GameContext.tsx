import React, { createContext, useState, ReactNode, useEffect } from 'react';
import Axios from 'axios';
import { clientConfig } from './client.config';

function api(path: string) {
    return clientConfig.apiURL + path;
}

export const GameContext = createContext({
    isTriggered: false,
    setIsTriggered: (validation: boolean) => {},
    isTimeOut: false,
    setIsTimeOut: (validation: boolean) => {},
    sendScore: (score: number, precision: number, username: string) => {},
    getWordsSerie: () => {},
    randomWordsSerie: [],
    getScore: async () => {
        const response = await Axios.get(api('/high-score-point'))
        return response.data
    },
    fullWidth: false,
    setFullWidth: (width: boolean) => {},
    sendNewWord: (newWord: string, point: number) => {},
    notifyScoreList: false
});


type Props = {
    children: ReactNode;
}

const GameWrapper: React.FC<Props> = ({children}) => {

    const [isTriggered, setIsTriggered] = useState(false);
    const [isTimeOut, setIsTimeOut] = useState(false);
    const [notifyScoreList, setNotifyScoreList] = useState(false);
    const [randomWordsSerie, setRandomWordsSerie] = useState([]);
    const [fullWidth, setFullWidth] = useState(false);
    const [highScoreList, sethighScoreList] = useState([]);

    Axios.defaults.withCredentials = true;

    useEffect(() => {
        if(!isTriggered) {
            getWordsSerie();
        }
    }, [isTriggered]);

    const sendScore = async (score: number, precision: number, username: string) => {

        const dateNow = new Date();
        const maDate = dateNow.getFullYear() + '-' + (dateNow.getMonth() +1) + '-' + dateNow.getDate() + ' ' + dateNow.getHours() + ':' + dateNow.getMinutes() + ':' + dateNow.getSeconds();
        await Axios.post(api('/score'), {
            dateScore: maDate, 
            points: score, 
            accuracy: precision, 
            username: username, 
            idType: 1
        });
        setNotifyScoreList(!notifyScoreList);
    }

    // Get Words for the game
    const getWordsSerie = async () => {
        try {
           const response = await Axios.get(api('/wordsSerie'));
            setRandomWordsSerie(response.data); 
        } catch (error) {
            console.log(error)
        }
        
    };
    
    const getScore = async () => {
        try {
            const response = await Axios.get(api('/high-score-point'))
        return response.data
        } catch (error) {
            console.log(error)
        }
    };

    const sendNewWord = async (newWord: string, point: number) => {
        try {
            const response = await Axios.post(api('/word'), {
                word: newWord, 
                point: point 
            })
            return response
        } catch (error) {
            return null
        }
    }
    
    const value = {
        isTriggered, 
        setIsTriggered, 
        isTimeOut, 
        setIsTimeOut, 
        sendScore, 
        getWordsSerie, 
        randomWordsSerie, 
        getScore, 
        fullWidth, 
        setFullWidth, 
        sendNewWord,
        highScoreList,
        notifyScoreList
    };

    return (
        <GameContext.Provider value={value}>
            {children}
        </GameContext.Provider>
    );

}

export default GameWrapper;
