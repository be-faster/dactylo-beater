/* eslint-disable react-hooks/exhaustive-deps */
import React, { createContext, useEffect, useState, ReactNode } from 'react';
import Axios from 'axios';
import { clientConfig } from './client.config';

function api(path: string) {
    return clientConfig.apiURL + path;
}

export const UserContext = createContext({
    username: '',
    isConnected: false,
    password: '',
    email: '',
    errorConnection: '',
    userScore: [],
    setUsername: (username: string) => {},
    setSession: (validation: boolean) => {},
    setPassword: (password: string) =>{},
    setEmail: (email: string) =>{},
    login: () => {},
    logout: () => {},
    register: () => {},
    hydrate: () => {},
    addNewFriend: (friendName: string) => {},
    getUserScore: async () => {
        const response = await Axios.get(api('/score_user'));
        return response.data;
    },
    getUserFriendHighScore: async () => {
        try {
            const response = await Axios.get(api('/friends-high-score'));
            return response.data;
        } catch (error) {
            return error;
        }
    },
    getUserFriendHighAccuracy: async () => {
        try {
            const response = await Axios.get(api('/friends-high-accuracy'));
            return response.data;
        } catch (error) {
            return error;
        }
    } 
});

type Props = {
    children: ReactNode;
}

const AuthWrapper: React.FC<Props> = ({children}) => {
    const [username, setUsername] = useState('');
    const [isConnected, setSession] = useState(false);
    const [password, setPassword] = useState('');
    const [userScore, setUserScore] = useState([]);
    const [email, setEmail] = useState('');
    const [errorConnection, setErrorConnection] = useState('');
    
    Axios.defaults.withCredentials = true;

    useEffect(() => {
        hydrate()
    }, [isConnected]);

    const eraseUser = () => {
        setUsername('');
        setPassword('');
        setEmail('');
        setSession(false);
    }

    const hydrate = async () => {
        try {
            console.log('Hydrate: ', isConnected)
            const response = await Axios.get(api('/me'))
            if(response.data.result[0].username) {
                setSession(true);
                setUsername(response.data.result[0].username);
            } else {
                eraseUser();
            }
        } catch(error) {
              console.log(error);
              eraseUser();
        }
    }


    // // need to give props to this function
    const login = async () => {
        try {
            const response = await Axios.post(api('/login'),{username: username,password: password})
            if (response.data) {
                setSession(true);
            } else {
                setErrorConnection(response.data.Error);
            }
        } catch (error) {
            console.log(error);
        }
    }

    const logout = () => {
        Axios.post(api('/logout')).then((response) => {
            eraseUser();
        })
        .catch(function (error) {
            console.log(error);
          })
    }

    //need to give props to this function
    const register = () => {
        Axios.post(api('/register'), {username: username, password: password, mail: email}).then((response) => {
          console.log(response);
        });
    }

    const getUserScore = async () => {
        const response  = await Axios.get(api('/score_user'));
        return response.data;
    }

    const addNewFriend = async (usernameFriend: string) => {
        try {
            const response = await Axios.post(api('/friends'), {
                username: username,
                usernameFriend: usernameFriend
            });
            return response
        } catch (error) {
            return null;
        }
    }

    const getUserFriendHighScore = async () => {
        try {
            const response = await Axios.get(api('/friends-high-score'))
            return response.data;
        } catch (error) {
            return error;
        }
    }

    const getUserFriendHighAccuracy = async () => {
        try {
            const response = await Axios.get(api('/friends-high-accuracy'))
            return response.data;
        } catch (error) {
            return error;
        }
    }

    const value = { 
        username, 
        setUsername, 
        isConnected, 
        setSession,
        password,
        setPassword,
        email,
        setEmail,
        login,
        logout,
        register,
        hydrate,
        errorConnection,
        getUserScore,
        userScore,
        addNewFriend,
        getUserFriendHighScore,
        getUserFriendHighAccuracy
    };

    return (
        <UserContext.Provider value={value}>
            {children}
        </UserContext.Provider>
    );
}

export default AuthWrapper;
