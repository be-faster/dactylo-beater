const Dictionary = require('../../models/dictionary.models.js')

exports.getWordsSerie = async function getWordsSerie (req, res) {
  const result = await Dictionary.getWordsSerie()
  res.send(result)
}

