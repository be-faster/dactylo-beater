const Dictionary = require('../../models/dictionary.models.js')

exports.postWord = async function postWord (req, res) {
  const { word, point } = req.body

  const result = await Dictionary.postWord(word, point)
  res.send(result)
}
