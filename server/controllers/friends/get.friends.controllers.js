const Relationship = require('../../models/friends.models.js')

exports.getFriends = async function getFriends (req, res) {
  const userId = req.session.userId

  const result = await Relationship.getAllFriends(userId)
  res.send(result)
}

exports.getFriend = async function getFriend (req, res) {
  const idFriend = req.params.idFriend
  const userId = req.session.userId
  const result = await Relationship.getFriend(userId, idFriend)
  res.send(result)
}

exports.getFriendsHighScore = async function getFriendsHighScore (req, res) {
  const userId = req.session.userId

  const result = await Relationship.getFriendsHighScore(userId)
  res.send(result)
}

exports.getFriendsHighAccuracy = async function getFriendsHighAccuracy (req, res) {
  const userId = req.session.userId

  const result = await Relationship.getFriendsHighAccuracy(userId)
  res.send(result)
}
