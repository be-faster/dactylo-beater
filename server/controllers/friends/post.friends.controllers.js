const Relationship = require('../../models/friends.models.js')

exports.postRelationship = async function postRelationship (req, res) {
  const { usernameFriend } = req.body

  // Call the function to create a new relationship between two users
  const result = await Relationship.postFriend(req.session.userId, usernameFriend)
  res.send(result)
}
