const GameType = require('../../models/gameType.js')

exports.deleteGameType = async function getIdGame (req, res) {
  try {
    const id = req.params.idGameType
    const result = await GameType.getIdGame(id)
    res.send(result)
  } catch (err) {
    console.log(err)
    res.statusCode = 404
    res.send({ Message: 'Game Not Found' })
  }
}
