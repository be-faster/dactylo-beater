const GameType = require('../../models/gameType.js')

exports.getIdGame = async function getIdGame (req, res) {
  try {
    const id = req.params.idGameType
    const result = await GameType.getIdGame(id)
    res.send(result)
  } catch (err) {
    console.log(err)
    res.statusCode = 404
    res.send({ Message: 'Game Not Found' })
  }
}

exports.getAllGame = async function getAllGame (res) {
  try {
    const result = await GameType.getAllGame()
    res.send(result)
  } catch (err) {
    console.log(err)
    res.statusCode = 404
    res.send({ Message: 'No Game found' })
  }
}
