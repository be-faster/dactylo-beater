const GameType = require('../../models/gameType.js')

exports.postGameType = async function postGameType (req, res) {
  try {
    const { idGameType, nameGameType } = req.body

    // Call the function to create a new game type
    const result = await GameType.postGameType(idGameType, nameGameType)
    res.send(result)
  } catch (err) {
    // When the new game type is rejected
    console.log(err)
    res.statusCode = 404
    res.send({ Message: 'the id already exist' })
  }
}
