const Score = require('../../models/score.models.js')

exports.getHighScoreAccuracy = async function getHighScoreAccuracy (req, res) {
  // Call the function to get a score and send it
  const result = await Score.getHighScoreAccuracy()
  res.send(result)
}
