const Score = require('../../models/score.models.js')

exports.getHighScorePoint = async function getHighScorePoint (req, res) {
  // Call the function to get a score and send it
  const result = await Score.getHighScorePoint()
  res.send(result)
}
