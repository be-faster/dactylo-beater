const Score = require('../../models/score.models.js')

exports.getScoreFromUser = async function getScoreFromUser (req, res) {
  // Call the function to get a score and send it
  const result = await Score.getScoreFromUser(req.session.userId)
  res.send(result)
}
