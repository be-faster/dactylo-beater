const Score = require('../../models/score.models.js')

exports.postScore = async function postScore (req, res) {
  const { dateScore, points, accuracy, idType } = req.body

  // Call the function to insert a new score
  const result = await Score.postScore(dateScore, points, accuracy, idType, req.session.userId)
  res.send(result)
}
