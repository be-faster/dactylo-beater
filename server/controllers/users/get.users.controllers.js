const Users = require('../../models/users.models.js')

exports.getUsers = async function getUsers (req, res) {
  const result = await Users.getAllUsers()
  for (const user of result) {
    delete user.pwd
  }
  res.json(result)
}

exports.getUser = async function getUser (req, res) {
  const id = parseInt(req.params.id)
  // Call the function to get a user and send it
  const result = await Users.getUser(id)
  res.send(result)
}

exports.getMe = async function getMe (req, res) {
  // Call the function to create a new user and send the newly update user
  const result = await Users.getMe(req.session.userId)
  res.send(result)
}
