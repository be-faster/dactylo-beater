const User = require('../../models/users.models.js')

exports.authenticateUser = async function authenticateUser (req, res) {
  const { username, password } = req.body

  // Call the function to authenticate the user
  const result = await User.authenticateUser(username, password)
  if (result.error) {
    res.status(401).send(result)
    return
  }

  delete result.pwd

  // Set the session userId
  if (!req.session.userId) {
    req.session.userId = result.userId
  }
  res.send(result)
}

exports.registerUser = async function registerUser (req, res) {
  const { username, password, mail } = req.body
  const result = await User.registerUser(username, password, mail)
  delete result.password
  res.json(result)
}

exports.logoutUser = async function logoutUser (req, res) {
  // Call the function to logout the user
  req.session.destroy(() => {
    res.send({ Message: 'Logged out successfully' })
  })
}
