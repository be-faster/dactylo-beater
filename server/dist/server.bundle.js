/*
 * ATTENTION: The "eval" devtool has been used (maybe by default in mode: "development").
 * This devtool is not neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
/******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./controllers/dictionary/get.words.controllers.js":
/*!*********************************************************!*\
  !*** ./controllers/dictionary/get.words.controllers.js ***!
  \*********************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

eval("const Dictionary = __webpack_require__(/*! ../../models/dictionary.models.js */ \"./models/dictionary.models.js\"); // exports.getWords = async function getWords (req, res) {\n//   try {\n//     const result = await Dictionary.getAllWords()\n//     res.send(result)\n//   } catch (err) {\n//     console.log(err)\n//     res.status(500).send({ Error: err.message })\n//   }\n// }\n\n\nexports.getWordsSerie = async function getWordsSerie(req, res) {\n  const result = await Dictionary.getWordsSerie();\n  res.send(result);\n}; // exports.getWord = async function getWord (req, res) {\n//   try {\n//     const id = req.params.id\n//     const result = await Dictionary.getWord(id)\n//     res.send(result)\n//   } catch (err) {\n//     // When getWord is rejected\n//     console.log(err)\n//     res.status(500).send({ Error: err.message })\n//   }\n// }\n\n//# sourceURL=webpack://server-express/./controllers/dictionary/get.words.controllers.js?");

/***/ }),

/***/ "./controllers/dictionary/post.words.controllers.js":
/*!**********************************************************!*\
  !*** ./controllers/dictionary/post.words.controllers.js ***!
  \**********************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

eval("const Dictionary = __webpack_require__(/*! ../../models/dictionary.models.js */ \"./models/dictionary.models.js\");\n\nexports.postWord = async function postWord(req, res) {\n  const {\n    word,\n    point\n  } = req.body;\n  const result = await Dictionary.postWord(word, point);\n  res.send(result);\n};\n\n//# sourceURL=webpack://server-express/./controllers/dictionary/post.words.controllers.js?");

/***/ }),

/***/ "./controllers/friends/get.friends.controllers.js":
/*!********************************************************!*\
  !*** ./controllers/friends/get.friends.controllers.js ***!
  \********************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

eval("const Relationship = __webpack_require__(/*! ../../models/friends.models.js */ \"./models/friends.models.js\");\n\nexports.getFriends = async function getFriends(req, res) {\n  const userId = req.session.userId;\n  const result = await Relationship.getAllFriends(userId);\n  res.send(result);\n};\n\nexports.getFriend = async function getFriend(req, res) {\n  const idFriend = req.params.idFriend;\n  const userId = req.session.userId;\n  const result = await Relationship.getFriend(userId, idFriend);\n  res.send(result);\n};\n\nexports.getFriendsHighScore = async function getFriendsHighScore(req, res) {\n  const userId = req.session.userId;\n  const result = await Relationship.getFriendsHighScore(userId);\n  res.send(result);\n};\n\nexports.getFriendsHighAccuracy = async function getFriendsHighAccuracy(req, res) {\n  const userId = req.session.userId;\n  const result = await Relationship.getFriendsHighAccuracy(userId);\n  res.send(result);\n};\n\n//# sourceURL=webpack://server-express/./controllers/friends/get.friends.controllers.js?");

/***/ }),

/***/ "./controllers/friends/post.friends.controllers.js":
/*!*********************************************************!*\
  !*** ./controllers/friends/post.friends.controllers.js ***!
  \*********************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

eval("const Relationship = __webpack_require__(/*! ../../models/friends.models.js */ \"./models/friends.models.js\");\n\nexports.postRelationship = async function postRelationship(req, res) {\n  const {\n    usernameFriend\n  } = req.body; // Call the function to create a new relationship between two users\n\n  const result = await Relationship.postFriend(req.session.userId, usernameFriend);\n  res.send(result);\n};\n\n//# sourceURL=webpack://server-express/./controllers/friends/post.friends.controllers.js?");

/***/ }),

/***/ "./controllers/score/get.highaccuracy.controllers.js":
/*!***********************************************************!*\
  !*** ./controllers/score/get.highaccuracy.controllers.js ***!
  \***********************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

eval("const Score = __webpack_require__(/*! ../../models/score.models.js */ \"./models/score.models.js\");\n\nexports.getHighScoreAccuracy = async function getHighScoreAccuracy(req, res) {\n  // Call the function to get a score and send it\n  const result = await Score.getHighScoreAccuracy();\n  res.send(result);\n};\n\n//# sourceURL=webpack://server-express/./controllers/score/get.highaccuracy.controllers.js?");

/***/ }),

/***/ "./controllers/score/get.highscore.controllers.js":
/*!********************************************************!*\
  !*** ./controllers/score/get.highscore.controllers.js ***!
  \********************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

eval("const Score = __webpack_require__(/*! ../../models/score.models.js */ \"./models/score.models.js\");\n\nexports.getHighScorePoint = async function getHighScorePoint(req, res) {\n  // Call the function to get a score and send it\n  const result = await Score.getHighScorePoint();\n  res.send(result);\n};\n\n//# sourceURL=webpack://server-express/./controllers/score/get.highscore.controllers.js?");

/***/ }),

/***/ "./controllers/score/get.userscore.controllers.js":
/*!********************************************************!*\
  !*** ./controllers/score/get.userscore.controllers.js ***!
  \********************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

eval("const Score = __webpack_require__(/*! ../../models/score.models.js */ \"./models/score.models.js\");\n\nexports.getScoreFromUser = async function getScoreFromUser(req, res) {\n  // Call the function to get a score and send it\n  const result = await Score.getScoreFromUser(req.session.userId);\n  res.send(result);\n};\n\n//# sourceURL=webpack://server-express/./controllers/score/get.userscore.controllers.js?");

/***/ }),

/***/ "./controllers/score/post.score.controllers.js":
/*!*****************************************************!*\
  !*** ./controllers/score/post.score.controllers.js ***!
  \*****************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

eval("const Score = __webpack_require__(/*! ../../models/score.models.js */ \"./models/score.models.js\");\n\nexports.postScore = async function postScore(req, res) {\n  const {\n    dateScore,\n    points,\n    accuracy,\n    idType\n  } = req.body; // Call the function to insert a new score\n\n  const result = await Score.postScore(dateScore, points, accuracy, idType, req.session.userId);\n  res.send(result);\n};\n\n//# sourceURL=webpack://server-express/./controllers/score/post.score.controllers.js?");

/***/ }),

/***/ "./controllers/users/get.users.controllers.js":
/*!****************************************************!*\
  !*** ./controllers/users/get.users.controllers.js ***!
  \****************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

eval("const Users = __webpack_require__(/*! ../../models/users.models.js */ \"./models/users.models.js\");\n\nexports.getUsers = async function getUsers(req, res) {\n  const result = await Users.getAllUsers();\n\n  for (const user of result) {\n    delete user.pwd;\n  }\n\n  res.json(result);\n};\n\nexports.getUser = async function getUser(req, res) {\n  const id = parseInt(req.params.id); // Call the function to get a user and send it\n\n  const result = await Users.getUser(id);\n  res.send(result);\n};\n\nexports.getMe = async function getMe(req, res) {\n  // Call the function to create a new user and send the newly update user\n  const result = await Users.getMe(req.session.userId);\n  res.send(result);\n};\n\n//# sourceURL=webpack://server-express/./controllers/users/get.users.controllers.js?");

/***/ }),

/***/ "./controllers/users/post.users.controllers.js":
/*!*****************************************************!*\
  !*** ./controllers/users/post.users.controllers.js ***!
  \*****************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

eval("const User = __webpack_require__(/*! ../../models/users.models.js */ \"./models/users.models.js\");\n\nexports.authenticateUser = async function authenticateUser(req, res) {\n  const {\n    username,\n    password\n  } = req.body; // Call the function to authenticate the user\n\n  const result = await User.authenticateUser(username, password);\n\n  if (result.error) {\n    res.status(401).send(result);\n    return;\n  }\n\n  delete result.pwd; // Set the session userId\n\n  if (!req.session.userId) {\n    req.session.userId = result.userId;\n  }\n\n  res.send(result);\n};\n\nexports.registerUser = async function registerUser(req, res) {\n  const {\n    username,\n    password,\n    mail\n  } = req.body;\n  const result = await User.registerUser(username, password, mail);\n  delete result.password;\n  res.json(result);\n};\n\nexports.logoutUser = async function logoutUser(req, res) {\n  // Call the function to logout the user\n  req.session.destroy(() => {\n    res.send({\n      Message: 'Logged out successfully'\n    });\n  });\n};\n\n//# sourceURL=webpack://server-express/./controllers/users/post.users.controllers.js?");

/***/ }),

/***/ "./local.server.config.js":
/*!********************************!*\
  !*** ./local.server.config.js ***!
  \********************************/
/***/ ((module) => {

eval("module.exports = {\n  SESSION_SECRET: 'randomly generated string',\n  MYSQL_HOST: 'aws-dactylobeater.c3hbqf2e4ykf.us-east-1.rds.amazonaws.com',\n  MYSQL_USER: 'admin',\n  MYSQL_DB: 'dactylobeaterdb',\n  MYSQL_PASSWORD: '6=#@u4dgU8Xmr#A9'\n};\n\n//# sourceURL=webpack://server-express/./local.server.config.js?");

/***/ }),

/***/ "./middlewares/errors.js":
/*!*******************************!*\
  !*** ./middlewares/errors.js ***!
  \*******************************/
/***/ ((module) => {

eval("function errors(fn) {\n  async function middleware(req, res, next) {\n    try {\n      await fn(req, res, next);\n    } catch (err) {\n      console.log('Request errored', err); // eslint-disable-line\n\n      res.sendStatus(500);\n    }\n  }\n\n  return middleware;\n}\n\nmodule.exports = errors;\n\n//# sourceURL=webpack://server-express/./middlewares/errors.js?");

/***/ }),

/***/ "./middlewares/is-connected.js":
/*!*************************************!*\
  !*** ./middlewares/is-connected.js ***!
  \*************************************/
/***/ ((module) => {

eval("function isConnected(req, res, next) {\n  if (req.session.userId) {\n    next();\n    return;\n  }\n\n  res.statusCode = 401;\n  res.send({\n    Error: 'Not connected'\n  });\n}\n\nmodule.exports = isConnected;\n\n//# sourceURL=webpack://server-express/./middlewares/is-connected.js?");

/***/ }),

/***/ "./models/dictionary.models.js":
/*!*************************************!*\
  !*** ./models/dictionary.models.js ***!
  \*************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

eval("const mySqlStore = __webpack_require__(/*! ../mysql-store.js */ \"./mysql-store.js\");\n\nclass Dictionary {\n  /**\n   *\n   * @param { string } word Word we want to add\n   * @param { number } point point for the word\n   * @return word object posted\n   */\n  static async postWord(word, point) {\n    await mySqlStore.connection.awaitQuery(`INSERT INTO Dictionary(word, point) VALUES\n      (?, ?)`, [word, point]);\n    return {\n      word,\n      point\n    };\n  }\n  /**\n   * @summary Function to get a serie of word (default is 15 words)\n   */\n\n\n  static async getWordsSerie() {\n    const qWords = 'SELECT * FROM Dictionary ORDER BY RAND() LIMIT 200';\n    const result = await mySqlStore.connection.awaitQuery(qWords);\n    return result;\n  }\n\n}\n\nmodule.exports = Dictionary;\n\n//# sourceURL=webpack://server-express/./models/dictionary.models.js?");

/***/ }),

/***/ "./models/friends.models.js":
/*!**********************************!*\
  !*** ./models/friends.models.js ***!
  \**********************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

eval("const mySqlStore = __webpack_require__(/*! ../mysql-store.js */ \"./mysql-store.js\");\n\nclass Relationship {\n  /**\n   * @summary Function to get all user's relationship from the database.\n   * @param {number} userId Id of the user.\n   * @returns {Promise} Return all friends of idUser\n   */\n  static async getAllFriends(userId) {\n    const result = await mySqlStore.connection.awaitQuery('SELECT us1.ID_User ID_User1, us1.username username1, us2.ID_User ID_User2, us2.username as username2 from Users as us1 JOIN Friends on us1.ID_User = Friends.ID_User1 JOIN Users as us2 on us2.ID_User = Friends.ID_User2 WHERE us1.ID_User = ? OR us2.ID_User = ?; ', [userId, userId]); // Get all the friends of the user\n\n    const friends = [];\n\n    for (const user of result) {\n      if (user.ID_User1 === userId) {\n        friends.push({\n          username: user.username2,\n          id: user.ID_User2\n        });\n      } else {\n        friends.push({\n          username: user.username1,\n          id: user.ID_User1\n        });\n      }\n    }\n\n    return friends;\n  }\n  /**\n    * @summary Function to get a relationship between two users.\n    * @param {number} idUser1 Id of the user.\n    * @param {number} idUser2 Id of the firend.\n    * @returns {Promise} The relation between 2 users if it exists\n    */\n\n\n  static async getFriend(idUser1, idUser2) {\n    const result = await mySqlStore.connection.awaitQuery('SELECT * FROM Friends WHERE (ID_User1=? AND ID_User2=?) OR (ID_User1=? AND ID_User2=?)', [idUser1, idUser2, idUser2, idUser1]);\n\n    if (result.length === 0) {\n      return {\n        Message: 'Not in your friends list'\n      };\n    }\n\n    return result;\n  }\n  /**\n    * @summary Function to create a new relationship.\n    * @param {number} userId username of the user.\n    * @param {string} usernameFriend username of the friend.\n    * @returns {Promise}\n    */\n\n\n  static async postFriend(userId, usernameFriend) {\n    const result = await mySqlStore.connection.awaitQuery(`INSERT INTO Friends(ID_User1, ID_User2) VALUES\n      (?, (select ID_User from Users where username = ?))`, [userId, usernameFriend]);\n\n    if (result.affectedRows === 1) {\n      return {\n        userId,\n        usernameFriend\n      };\n    }\n  } // /**\n  //   * @summary Function to delete a relationship between two users.\n  //   * @param {string} idUser1  Id of the user.\n  //   * @param {string} idUser2  Id of the user.\n  //   * @returns {Promise}\n  //   */\n  // static async deleteFriend (idUser1, idUser2) {\n  //   return new Promise((resolve, reject) => {\n  //     mySqlStore.connection.query('DELETE FROM Friends WHERE (ID_User1=? and ID_User2=?) or (ID_User1=? and ID_User2=?)', [parseFloat(idUser1), parseFloat(idUser2), parseFloat(idUser2), parseFloat(idUser1)], (err, result) => {\n  //       return err ? reject(err) : resolve({ idUser1, idUser2, deleted: true })\n  //     })\n  //   })\n  // }\n\n  /**\n    * @summary Function to delete a relationship between two users.\n    * @param {number} idUser  Id of the user.\n    * @returns {Promise}\n    */\n\n\n  static async getFriendsHighScore(idUser) {\n    const friendsList = await Relationship.getAllFriends(idUser);\n    const friendsHighScore = [];\n    const result = await mySqlStore.connection.awaitQuery('SELECT Users.ID_User, Users.username, MAX(points) highPoint, accuracy, dateScore from Score JOIN Users on Score.ID_User = Users.ID_User GROUP BY ID_User;');\n\n    for (const user of friendsList) {\n      const res = result.find(u => u.ID_User === user.id);\n\n      if (res) {\n        friendsHighScore.push(res);\n      } else {\n        // Reset the ID_User field\n        const noScoreUser = { ...user,\n          ID_User: user.id\n        };\n        delete noScoreUser.id;\n        friendsHighScore.push(noScoreUser);\n      }\n    }\n\n    return {\n      friendsHighScore\n    };\n  }\n  /**\n    * @summary Function to delete a relationship between two users.\n    * @param {number} idUser  Id of the user.\n    * @returns {Promise}\n    */\n\n\n  static async getFriendsHighAccuracy(idUser) {\n    const friendsList = await Relationship.getAllFriends(idUser);\n    const friendsHighAccuracy = [];\n    const result = await mySqlStore.connection.awaitQuery('SELECT Users.ID_User, Users.username, MAX(accuracy) highAccuracy, points, dateScore from Score JOIN Users on Score.ID_User = Users.ID_User GROUP BY ID_User;');\n\n    for (const user of friendsList) {\n      const res = result.find(u => u.ID_User === user.id);\n\n      if (res) {\n        friendsHighAccuracy.push(res);\n      } else {\n        // Reset the ID_User field\n        const noScoreUser = { ...user,\n          ID_User: user.id\n        };\n        delete noScoreUser.id;\n        friendsHighAccuracy.push(noScoreUser);\n      }\n    }\n\n    return {\n      friendsHighAccuracy\n    };\n  }\n\n}\n\nmodule.exports = Relationship;\n\n//# sourceURL=webpack://server-express/./models/friends.models.js?");

/***/ }),

/***/ "./models/score.models.js":
/*!********************************!*\
  !*** ./models/score.models.js ***!
  \********************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

eval("const mySqlStore = __webpack_require__(/*! ../mysql-store.js */ \"./mysql-store.js\");\n\nclass Score {\n  /**\n    * @summary Function to get user's score from the database.\n    * @param {number} idUser Id of the user to get.\n    * @returns {Promise}\n    */\n  static async getScoreFromUser(idUser) {\n    const result = await mySqlStore.connection.awaitQuery('SELECT * FROM Score WHERE ID_User=?;', [idUser]);\n    return result;\n  }\n  /** @summary Function to get the top 10 best score (points)\n    * @returns {Promise}\n    */\n\n\n  static async getHighScorePoint() {\n    const result = await mySqlStore.connection.awaitQuery('SELECT user.username, score.dateScore, score.points, game.name FROM Users AS user \\n' + 'RIGHT OUTER JOIN Score AS score ON user.ID_User=score.ID_User \\n' + 'LEFT OUTER JOIN GameType AS game on score.ID_Type=game.ID_Type ORDER BY score.points DESC limit 10');\n    return result;\n  }\n  /** @summary Function to get the top 10 best score (accuracy)\n    * @returns {Promise}\n    */\n\n\n  static async getHighScoreAccuracy() {\n    const result = await mySqlStore.connection.awaitQuery('SELECT user.username, score.dateScore, score.accuracy, game.name FROM Users AS user \\n' + 'RIGHT OUTER JOIN Score AS score ON user.ID_User=score.ID_User \\n' + 'LEFT OUTER JOIN GameType AS game ON score.ID_Type=game.ID_Type ORDER BY score.accuracy DESC limit 10');\n    return result;\n  }\n  /**\n    * @summary Function to create a new score.\n    * @param {Date} dateScore  Current Date.\n    * @param {number} points  Total Points.\n    * @param {number} accuracy precision.\n    * @param {number} idType Foreign Key of GameType.\n    * @param {number} userId users session id\n    * @returns {Promise}\n    */\n\n\n  static async postScore(dateScore, points, accuracy, idType, userId) {\n    await mySqlStore.connection.awaitQuery(`INSERT INTO Score(dateScore, points, accuracy, ID_User, ID_Type) VALUES\n      (?, ?, ?, ?, ?)`, [dateScore, points, accuracy, userId, idType]);\n    return {\n      dateScore,\n      points,\n      accuracy,\n      userId,\n      idType\n    };\n  }\n\n}\n\nmodule.exports = Score;\n\n//# sourceURL=webpack://server-express/./models/score.models.js?");

/***/ }),

/***/ "./models/users.models.js":
/*!********************************!*\
  !*** ./models/users.models.js ***!
  \********************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

eval("const mySqlStore = __webpack_require__(/*! ../mysql-store.js */ \"./mysql-store.js\");\n\nconst bcrypt = __webpack_require__(/*! bcrypt */ \"bcrypt\");\n\nclass User {\n  /**\n   * @summary Function to get all the users from the database.\n   * @returns {Promise}\n   */\n  static async getAllUsers() {\n    const result = await mySqlStore.connection.awaitQuery('SELECT id_user, username, pwd FROM Users');\n    return result;\n  }\n  /**\n    * @summary Function to get a user by id.\n    * @param {number} id Id of the user to get.\n    * @returns {Promise}\n    */\n\n\n  static async getUser(id) {\n    const result = await mySqlStore.connection.awaitQuery('SELECT ID_User, username, pwd FROM Users WHERE ID_User=?', [id]);\n    return result;\n  } // /**\n  //   * @summary Function to delete a user by id.\n  //   * @param {number} id Id of the user we want to delete.\n  //   * @returns {Promise}\n  //   */\n  // static async deleteUser (id) {\n  //   const result = await mySqlStore.connection.awaitQuery('DELETE FROM Users WHERE ID_User=?', [id])\n  //   return result\n  // }\n  // /**\n  //   * @summary Function to update a user.\n  //   * @param {number} id\n  //   * @param {string} username\n  //   * @param {string} password\n  //   */\n  // static async putUser (id, username, password) {\n  //   const result = await mySqlStore.connection.awaitQuery('UPDATE Users SET username=?, pwd=? WHERE ID_User=?', [username, password, id])\n  //   return result\n  // }\n\n\n  static async authenticateUser(username, pwd) {\n    const result = await mySqlStore.connection.awaitQuery('SELECT * FROM Users WHERE username = ?', [username]);\n\n    if (result.length > 0) {\n      const response = await bcrypt.compare(pwd, result[0].pwd);\n\n      if (response) {\n        // Auth réussi\n        const userId = result[0].ID_User;\n        return {\n          response,\n          userId,\n          username,\n          pwd\n        };\n      } else {\n        // Si le mot de passe est mauvais\n        return {\n          error: 'Mauvais Mot de passe !'\n        };\n      }\n    } else {\n      // Si l'utilisateur n'existe pas\n      return {\n        error: 'L\\'utilisateur n\\'existe pas'\n      };\n    }\n  }\n  /**\n    * @summary Function to create a new user.\n    * @param {string} username  New username.\n    * @param {string} pwd  New password.\n    * @param {string} mail New email\n    * @returns {Promise}\n    */\n\n\n  static async registerUser(username, pwd, mail) {\n    const saltRounds = 10; // On utilise le bcrypt pour crypter notre mot de passe afin d'améliorer la sécurité des comptes utilisateurs.\n\n    const hashPassword = await new Promise((resolve, reject) => {\n      bcrypt.hash(pwd, saltRounds, (err, hash) => {\n        if (err) {\n          reject(new Error(`${err}`));\n        } else {\n          resolve(hash);\n        }\n      });\n    }); // Une fois le hashage terminé, on insère dans notre base de données avec notre login\n\n    const result = await mySqlStore.connection.awaitQuery('INSERT INTO Users(username, pwd, mail) VALUES (?, ?, ?)', [username, hashPassword, mail]);\n\n    if (result.affectedRows === 1) {\n      // Une fois le hashage terminé, on insère dans notre base de données avec notre login\n      const resultId = await mySqlStore.connection.awaitQuery('SELECT ID_User FROM Users WHERE username = ?', [username]);\n      return {\n        userId: resultId[0].ID_User,\n        new_user: username,\n        password: pwd,\n        mail: mail\n      };\n    }\n  }\n  /**\n   *\n   * @param {number} id\n   */\n\n\n  static async getMe(id) {\n    const result = await mySqlStore.connection.awaitQuery('SELECT ID_User, username FROM Users WHERE ID_User=?', [id]);\n    return result;\n  }\n\n}\n\nmodule.exports = User;\n\n//# sourceURL=webpack://server-express/./models/users.models.js?");

/***/ }),

/***/ "./mysql-store.js":
/*!************************!*\
  !*** ./mysql-store.js ***!
  \************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

eval("const mysql = __webpack_require__(/*! mysql-await */ \"mysql-await\");\n\nclass MySqlStore {\n  async init(config) {\n    this.connection = await mysql.createConnection({\n      host: config.host,\n      user: config.user,\n      password: config.password,\n      database: config.database\n    });\n  }\n\n}\n\nmodule.exports = new MySqlStore();\n\n//# sourceURL=webpack://server-express/./mysql-store.js?");

/***/ }),

/***/ "./routes/api.routes.js":
/*!******************************!*\
  !*** ./routes/api.routes.js ***!
  \******************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

eval("const router = __webpack_require__(/*! express */ \"express\").Router();\n\nconst userRoute = __webpack_require__(/*! ./users.routes.js */ \"./routes/users.routes.js\");\n\nconst dictionaryRoute = __webpack_require__(/*! ./dictionary.routes.js */ \"./routes/dictionary.routes.js\");\n\nconst scoreRoute = __webpack_require__(/*! ./score.routes.js */ \"./routes/score.routes.js\");\n\nconst relationshipRoute = __webpack_require__(/*! ./friends.routes.js */ \"./routes/friends.routes.js\");\n/**\n * Router for the configuration of each route\n */\n\n\nrouter.use('/', userRoute);\nrouter.use('/', dictionaryRoute);\nrouter.use('/', scoreRoute);\nrouter.use('/', relationshipRoute);\nmodule.exports = router;\n\n//# sourceURL=webpack://server-express/./routes/api.routes.js?");

/***/ }),

/***/ "./routes/dictionary.routes.js":
/*!*************************************!*\
  !*** ./routes/dictionary.routes.js ***!
  \*************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

eval("const router = __webpack_require__(/*! express */ \"express\").Router();\n\nconst dictionaryGetController = __webpack_require__(/*! ../controllers/dictionary/get.words.controllers.js */ \"./controllers/dictionary/get.words.controllers.js\");\n\nconst dictionaryPostController = __webpack_require__(/*! ../controllers/dictionary/post.words.controllers.js */ \"./controllers/dictionary/post.words.controllers.js\");\n\nconst errors = __webpack_require__(/*! ../middlewares/errors */ \"./middlewares/errors.js\"); // Configuration of dictionary Route\n\n\nrouter.get('/wordsSerie', errors(dictionaryGetController.getWordsSerie));\nrouter.post('/word', errors(dictionaryPostController.postWord));\nmodule.exports = router;\n\n//# sourceURL=webpack://server-express/./routes/dictionary.routes.js?");

/***/ }),

/***/ "./routes/friends.routes.js":
/*!**********************************!*\
  !*** ./routes/friends.routes.js ***!
  \**********************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

eval("const router = __webpack_require__(/*! express */ \"express\").Router();\n\nconst friendsGetController = __webpack_require__(/*! ../controllers/friends/get.friends.controllers.js */ \"./controllers/friends/get.friends.controllers.js\");\n\nconst friendsPostController = __webpack_require__(/*! ../controllers/friends/post.friends.controllers.js */ \"./controllers/friends/post.friends.controllers.js\");\n\nconst isConnected = __webpack_require__(/*! ../middlewares/is-connected */ \"./middlewares/is-connected.js\");\n\nconst errors = __webpack_require__(/*! ../middlewares/errors */ \"./middlewares/errors.js\"); // Configuration of relationship Route\n\n\nrouter.get('/friends', isConnected, errors(friendsGetController.getFriends));\nrouter.get('/friends-high-score', isConnected, errors(friendsGetController.getFriendsHighScore));\nrouter.get('/friends-high-accuracy', isConnected, errors(friendsGetController.getFriendsHighAccuracy));\nrouter.get('/friend/:idFriend', isConnected, errors(friendsGetController.getFriend));\nrouter.post('/friends', isConnected, errors(friendsPostController.postRelationship));\nmodule.exports = router;\n\n//# sourceURL=webpack://server-express/./routes/friends.routes.js?");

/***/ }),

/***/ "./routes/score.routes.js":
/*!********************************!*\
  !*** ./routes/score.routes.js ***!
  \********************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

eval("const router = __webpack_require__(/*! express */ \"express\").Router();\n\nconst scoreGetController = __webpack_require__(/*! ../controllers/score/get.userscore.controllers */ \"./controllers/score/get.userscore.controllers.js\");\n\nconst scoreGethighScoreController = __webpack_require__(/*! ../controllers/score/get.highscore.controllers.js */ \"./controllers/score/get.highscore.controllers.js\");\n\nconst scoreGethighAccuracyController = __webpack_require__(/*! ../controllers/score/get.highaccuracy.controllers.js */ \"./controllers/score/get.highaccuracy.controllers.js\");\n\nconst scorePostController = __webpack_require__(/*! ../controllers/score/post.score.controllers.js */ \"./controllers/score/post.score.controllers.js\");\n\nconst isConnected = __webpack_require__(/*! ../middlewares/is-connected */ \"./middlewares/is-connected.js\");\n\nconst errors = __webpack_require__(/*! ../middlewares/errors */ \"./middlewares/errors.js\"); // Configuration of score Route\n\n\nrouter.get('/high-score-point/', isConnected, errors(scoreGethighScoreController.getHighScorePoint));\nrouter.get('/high-score-accuracy/', isConnected, errors(scoreGethighAccuracyController.getHighScoreAccuracy));\nrouter.get('/score_user', isConnected, errors(scoreGetController.getScoreFromUser));\nrouter.post('/score', isConnected, errors(scorePostController.postScore));\nmodule.exports = router;\n\n//# sourceURL=webpack://server-express/./routes/score.routes.js?");

/***/ }),

/***/ "./routes/users.routes.js":
/*!********************************!*\
  !*** ./routes/users.routes.js ***!
  \********************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

eval("const router = __webpack_require__(/*! express */ \"express\").Router();\n\nconst userGetController = __webpack_require__(/*! ../controllers/users/get.users.controllers.js */ \"./controllers/users/get.users.controllers.js\");\n\nconst userPostController = __webpack_require__(/*! ../controllers/users/post.users.controllers.js */ \"./controllers/users/post.users.controllers.js\");\n\nconst isConnected = __webpack_require__(/*! ../middlewares/is-connected */ \"./middlewares/is-connected.js\");\n\nconst error = __webpack_require__(/*! ../middlewares/errors */ \"./middlewares/errors.js\"); // Configuration of users Route\n\n\nrouter.get('/users', isConnected, error(userGetController.getUsers));\nrouter.get('/user/:id', isConnected, error(userGetController.getUser));\nrouter.get('/me', isConnected, error(userGetController.getMe));\nrouter.post('/login', error(userPostController.authenticateUser));\nrouter.post('/register', error(userPostController.registerUser));\nrouter.post('/logout', isConnected, error(userPostController.logoutUser));\nmodule.exports = router;\n\n//# sourceURL=webpack://server-express/./routes/users.routes.js?");

/***/ }),

/***/ "./server.config.js":
/*!**************************!*\
  !*** ./server.config.js ***!
  \**************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

eval("let existingConf;\n\ntry {\n  existingConf = __webpack_require__(/*! ./local.server.config.js */ \"./local.server.config.js\");\n} catch (err) {\n  existingConf = {};\n}\n\nfunction e(param) {\n  return process.env[param] || existingConf[param] || '';\n}\n\nmodule.exports.SESSION_SECRET = e('SESSION_SECRET');\nmodule.exports.mysql = {\n  user: e('MYSQL_USER'),\n  host: e('MYSQL_HOST'),\n  database: e('MYSQL_DB'),\n  password: e('MYSQL_PASSWORD')\n};\n\n//# sourceURL=webpack://server-express/./server.config.js?");

/***/ }),

/***/ "bcrypt":
/*!*************************!*\
  !*** external "bcrypt" ***!
  \*************************/
/***/ ((module) => {

"use strict";
eval("module.exports = require(\"bcrypt\");;\n\n//# sourceURL=webpack://server-express/external_%22bcrypt%22?");

/***/ }),

/***/ "cors":
/*!***********************!*\
  !*** external "cors" ***!
  \***********************/
/***/ ((module) => {

"use strict";
eval("module.exports = require(\"cors\");;\n\n//# sourceURL=webpack://server-express/external_%22cors%22?");

/***/ }),

/***/ "express":
/*!**************************!*\
  !*** external "express" ***!
  \**************************/
/***/ ((module) => {

"use strict";
eval("module.exports = require(\"express\");;\n\n//# sourceURL=webpack://server-express/external_%22express%22?");

/***/ }),

/***/ "express-session":
/*!**********************************!*\
  !*** external "express-session" ***!
  \**********************************/
/***/ ((module) => {

"use strict";
eval("module.exports = require(\"express-session\");;\n\n//# sourceURL=webpack://server-express/external_%22express-session%22?");

/***/ }),

/***/ "http":
/*!***********************!*\
  !*** external "http" ***!
  \***********************/
/***/ ((module) => {

"use strict";
eval("module.exports = require(\"http\");;\n\n//# sourceURL=webpack://server-express/external_%22http%22?");

/***/ }),

/***/ "https":
/*!************************!*\
  !*** external "https" ***!
  \************************/
/***/ ((module) => {

"use strict";
eval("module.exports = require(\"https\");;\n\n//# sourceURL=webpack://server-express/external_%22https%22?");

/***/ }),

/***/ "mysql-await":
/*!******************************!*\
  !*** external "mysql-await" ***!
  \******************************/
/***/ ((module) => {

"use strict";
eval("module.exports = require(\"mysql-await\");;\n\n//# sourceURL=webpack://server-express/external_%22mysql-await%22?");

/***/ }),

/***/ "socket.io":
/*!****************************!*\
  !*** external "socket.io" ***!
  \****************************/
/***/ ((module) => {

"use strict";
eval("module.exports = require(\"socket.io\");;\n\n//# sourceURL=webpack://server-express/external_%22socket.io%22?");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		if(__webpack_module_cache__[moduleId]) {
/******/ 			return __webpack_module_cache__[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
(() => {
/*!***************************!*\
  !*** ./server-express.js ***!
  \***************************/
eval("const express = __webpack_require__(/*! express */ \"express\");\n\nconst app = express();\n\nconst session = __webpack_require__(/*! express-session */ \"express-session\");\n\nconst mySqlStore = __webpack_require__(/*! ./mysql-store */ \"./mysql-store.js\");\n\nconst config = __webpack_require__(/*! ./server.config */ \"./server.config.js\");\n\nconst apiRouter = __webpack_require__(/*! ./routes/api.routes */ \"./routes/api.routes.js\");\n\nconst http = __webpack_require__(/*! http */ \"http\").createServer(app);\n\nconst https = __webpack_require__(/*! https */ \"https\"); // @ts-ignore\n\n\nconst io = __webpack_require__(/*! socket.io */ \"socket.io\")(http, {\n  cors: {\n    origin: '*'\n  }\n});\n\nconst port = process.env.PORT || 3002;\n\nconst cors = __webpack_require__(/*! cors */ \"cors\");\n\nlet listUsers = [];\nconst tabWords = [];\nmySqlStore.init(config.mysql);\napp.use(express.json());\napp.use(express.urlencoded({\n  extended: false\n}));\nio.on('connection', socket => {\n  socket.on('player-join', ({\n    username\n  }) => {\n    let condition = false;\n    listUsers.forEach(e => {\n      if (e.username === username) {\n        condition = true;\n      }\n    });\n\n    if (condition) {\n      io.emit('player-join', listUsers);\n      console.log('true', listUsers);\n    } else {\n      listUsers.push({\n        username: username,\n        isReady: false,\n        score: 0,\n        inputWord: ''\n      });\n      io.emit('player-join', listUsers);\n      console.log('false', listUsers);\n    }\n  });\n  socket.on('player-ready', ({\n    username\n  }) => {\n    let autorisationWord = false;\n    listUsers.forEach(e => {\n      if (e.username === username) {\n        e.isReady = true;\n      }\n    });\n\n    if (isAllReady()) {\n      autorisationWord = true;\n      io.emit('player-ready', autorisationWord);\n    }\n  });\n  socket.on('update-score', (score, username) => {\n    let endGame = false;\n    listUsers.forEach(e => {\n      if (e.username === username) {\n        e.score = score;\n\n        if (e.score >= 100) {\n          endGame = true;\n        }\n      }\n\n      if (endGame) {\n        listUsers = [];\n        e.score = 0;\n      }\n    });\n    io.emit('update-score', listUsers, endGame);\n  });\n  https.get('https://dactylobeater.website/api/wordsSerie', resp => {\n    resp.on('data', chunk => {\n      const tmp = JSON.parse(chunk.toString());\n\n      for (const i of tmp) {\n        tabWords.push(i);\n      }\n    });\n    resp.on('end', () => {\n      socket.on('random-word', ({\n        wantWord\n      }) => {\n        if (wantWord) {\n          const randomWordSocket = tabWords[Math.floor(Math.random() * tabWords.length)];\n          io.emit('random-word', randomWordSocket);\n          console.log('rand', randomWordSocket);\n        }\n      });\n    });\n  }).on('error', err => {\n    console.log('Error : ' + err.message);\n  });\n  socket.on('manual-disconnect', () => {\n    console.log('User disconnected');\n    listUsers = [];\n  });\n});\n\nconst isAllReady = () => {\n  let isReady = true;\n  console.log('tab user size :', listUsers.length);\n\n  if (listUsers.length === 1) {\n    isReady = false;\n  } else {\n    listUsers.forEach(e => {\n      console.log(e.username, ' ', e.isReady);\n      if (e.isReady === false) isReady = false;\n    });\n  }\n\n  console.log('isAllReady', isReady);\n  return isReady;\n};\n\napp.use(session({\n  secret: 'mysecret',\n  resave: false,\n  saveUninitialized: false,\n  proxy: true // cookie: {\n  //   sameSite: 'none',\n  //   secure: true\n  // }\n\n}));\napp.use(cors({\n  // origin: 'https://master.d2klb8lvyvw0d8.amplifyapp.com',\n  origin: 'http://localhost:3000',\n  // origin: 'https://dactylobeater.herokuapp.com',\n  credentials: true\n}));\napp.use('/api/', apiRouter);\nhttp.listen(port, () => {\n  console.log('Express Server is up !');\n});\n\n//# sourceURL=webpack://server-express/./server-express.js?");
})();

/******/ })()
;