function errors (fn) {
  async function middleware (req, res, next) {
    try {
      await fn(req, res, next)
    } catch (err) {
      console.log('Request errored', err) // eslint-disable-line
      res.sendStatus(500)
    }
  }
  return middleware
}

module.exports = errors
