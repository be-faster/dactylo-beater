function isConnected (req, res, next) {
  if (req.session.userId) {
    next()
    return
  }
  res.statusCode = 401
  res.send({ Error: 'Not connected' })
}

module.exports = isConnected
