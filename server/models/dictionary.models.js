const mySqlStore = require('../mysql-store.js')

class Dictionary {
  /**
   *
   * @param { string } word Word we want to add
   * @param { number } point point for the word
   * @return word object posted
   */

  static async postWord (word, point) {
    await mySqlStore.connection.awaitQuery(`INSERT INTO Dictionary(word, point) VALUES
      (?, ?)`, [word, point])
    return { word, point }
  }

  /**
   * @summary Function to get a serie of word (default is 15 words)
   */
  static async getWordsSerie () {
    const qWords = 'SELECT * FROM Dictionary ORDER BY RAND() LIMIT 200'
    const result = await mySqlStore.connection.awaitQuery(qWords)
    return result
  }
}

module.exports = Dictionary
