const mySqlStore = require('../mysql-store.js')

class Relationship {
  /**
   * @summary Function to get all user's relationship from the database.
   * @param {number} userId Id of the user.
   * @returns {Promise} Return all friends of idUser
   */
  static async getAllFriends (userId) {
    const result = await mySqlStore.connection.awaitQuery('SELECT us1.ID_User ID_User1, us1.username username1, us2.ID_User ID_User2, us2.username as username2 from Users as us1 JOIN Friends on us1.ID_User = Friends.ID_User1 JOIN Users as us2 on us2.ID_User = Friends.ID_User2 WHERE us1.ID_User = ? OR us2.ID_User = ?; ', [userId, userId])
    // Get all the friends of the user
    const friends = []
    for (const user of result) {
      if (user.ID_User1 === userId) {
        friends.push({ username: user.username2, id: user.ID_User2 })
      } else {
        friends.push({ username: user.username1, id: user.ID_User1 })
      }
    }
    return friends
  }

  /**
    * @summary Function to get a relationship between two users.
    * @param {number} idUser1 Id of the user.
    * @param {number} idUser2 Id of the firend.
    * @returns {Promise} The relation between 2 users if it exists
    */
  static async getFriend (idUser1, idUser2) {
    const result = await mySqlStore.connection.awaitQuery('SELECT * FROM Friends WHERE (ID_User1=? AND ID_User2=?) OR (ID_User1=? AND ID_User2=?)', [idUser1, idUser2, idUser2, idUser1])
    if (result.length === 0) {
      return { Message: 'Not in your friends list' }
    }
    return result
  }

  /**
    * @summary Function to create a new relationship.
    * @param {number} userId username of the user.
    * @param {string} usernameFriend username of the friend.
    * @returns {Promise}
    */

  static async postFriend (userId, usernameFriend) {
    const result = await mySqlStore.connection.awaitQuery(`INSERT INTO Friends(ID_User1, ID_User2) VALUES
      (?, (select ID_User from Users where username = ?))`, [userId, usernameFriend])
    if (result.affectedRows === 1) {
      return ({ userId, usernameFriend })
    }
  }

  // /**
  //   * @summary Function to delete a relationship between two users.
  //   * @param {string} idUser1  Id of the user.
  //   * @param {string} idUser2  Id of the user.
  //   * @returns {Promise}
  //   */
  // static async deleteFriend (idUser1, idUser2) {
  //   return new Promise((resolve, reject) => {
  //     mySqlStore.connection.query('DELETE FROM Friends WHERE (ID_User1=? and ID_User2=?) or (ID_User1=? and ID_User2=?)', [parseFloat(idUser1), parseFloat(idUser2), parseFloat(idUser2), parseFloat(idUser1)], (err, result) => {
  //       return err ? reject(err) : resolve({ idUser1, idUser2, deleted: true })
  //     })
  //   })
  // }

  /**
    * @summary Function to delete a relationship between two users.
    * @param {number} idUser  Id of the user.
    * @returns {Promise}
    */
  static async getFriendsHighScore (idUser) {
    const friendsList = await Relationship.getAllFriends(idUser)
    const friendsHighScore = []

    const result = await mySqlStore.connection.awaitQuery('SELECT Users.ID_User, Users.username, MAX(points) highPoint, accuracy, dateScore from Score JOIN Users on Score.ID_User = Users.ID_User GROUP BY ID_User;')
    for (const user of friendsList) {
      const res = result.find((u) => u.ID_User === user.id)
      if (res) {
        friendsHighScore.push(res)
      } else {
        // Reset the ID_User field
        const noScoreUser = { ...user, ID_User: user.id }
        delete noScoreUser.id
        friendsHighScore.push(noScoreUser)
      }
    }
    return { friendsHighScore }
  }

  /**
    * @summary Function to delete a relationship between two users.
    * @param {number} idUser  Id of the user.
    * @returns {Promise}
    */
  static async getFriendsHighAccuracy (idUser) {
    const friendsList = await Relationship.getAllFriends(idUser)
    const friendsHighAccuracy = []
    const result = await mySqlStore.connection.awaitQuery('SELECT Users.ID_User, Users.username, MAX(accuracy) highAccuracy, points, dateScore from Score JOIN Users on Score.ID_User = Users.ID_User GROUP BY ID_User;')
    for (const user of friendsList) {
      const res = result.find((u) => u.ID_User === user.id)
      if (res) {
        friendsHighAccuracy.push(res)
      } else {
        // Reset the ID_User field
        const noScoreUser = { ...user, ID_User: user.id }
        delete noScoreUser.id
        friendsHighAccuracy.push(noScoreUser)
      }
    }
    return { friendsHighAccuracy }
  }
}

module.exports = Relationship
