
const mySqlStore = require('../mysql-store.js')

class GameType {
  /**
    * @summary Function to get the type of the game from the database.
    * @param {number} idGameType Id of the score to get.
    * @returns {Promise}
    */
  static async getIdGame (idGameType) {
    return new Promise((resolve, reject) => {
      mySqlStore.connection.query('SELECT * FROM GameType WHERE ID_Type=?;', [idGameType], (err, result) => {
        return err ? reject(new Error(`${err}`)) : resolve(result)
      })
    })
  }
  /**
   * @summary Function to get all the Game from the database.
   * @returns {Promise}
   */

  static async getAllGame () {
    return new Promise((resolve, reject) => {
      const qUsers = 'SELECT ID_Type, name FROM GameType'
      mySqlStore.connection.query(qUsers, (err, result) => {
        return err ? reject(err) : resolve(result)
      })
    })
  }

  /**
    * @summary Function to delete a game type by id.
    * @param {string} idGameType Id of the game we want to delete.
    * @returns {Promise}
    */
  static async deleteGameType (idGameType) {
    return new Promise((resolve, reject) => {
      mySqlStore.connection.query('DELETE FROM GameType WHERE ID_Type=?', [parseFloat(idGameType)], (err, result) => {
        return err ? reject(err) : resolve(idGameType)
      })
    })
  }

  /**
    * @summary Function to create a new game type.
    * @param {string} idGameType
    * @param {string} nameGameType  New game type.
    * @returns {Promise}
    */

  static async postGameType (idGameType, nameGameType) {
    return new Promise((resolve, reject) => {
      mySqlStore.connection.query(`INSERT INTO gameType(ID_type, name) VALUES
        (?, ?)`, [idGameType, nameGameType], (err, result) => {
        return err ? reject(err) : resolve({ idGameType, nameGameType })
      })
    })
  }
}

module.exports = GameType
