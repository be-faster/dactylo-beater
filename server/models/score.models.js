const mySqlStore = require('../mysql-store.js')

class Score {
  /**
    * @summary Function to get user's score from the database.
    * @param {number} idUser Id of the user to get.
    * @returns {Promise}
    */
  static async getScoreFromUser (idUser) {
    const result = await mySqlStore.connection.awaitQuery('SELECT * FROM Score WHERE ID_User=?;', [idUser])
    return result
  }

  /** @summary Function to get the top 10 best score (points)
    * @returns {Promise}
    */
  static async getHighScorePoint () {
    const result = await mySqlStore.connection.awaitQuery('SELECT user.username, score.dateScore, score.points, game.name FROM Users AS user \n' +
      'RIGHT OUTER JOIN Score AS score ON user.ID_User=score.ID_User \n' +
      'LEFT OUTER JOIN GameType AS game on score.ID_Type=game.ID_Type ORDER BY score.points DESC limit 10')
    return result
  }

  /** @summary Function to get the top 10 best score (accuracy)
    * @returns {Promise}
    */
  static async getHighScoreAccuracy () {
    const result = await mySqlStore.connection.awaitQuery('SELECT user.username, score.dateScore, score.accuracy, game.name FROM Users AS user \n' +
    'RIGHT OUTER JOIN Score AS score ON user.ID_User=score.ID_User \n' +
    'LEFT OUTER JOIN GameType AS game ON score.ID_Type=game.ID_Type ORDER BY score.accuracy DESC limit 10')
    return result
  }

  /**
    * @summary Function to create a new score.
    * @param {Date} dateScore  Current Date.
    * @param {number} points  Total Points.
    * @param {number} accuracy precision.
    * @param {number} idType Foreign Key of GameType.
    * @param {number} userId users session id
    * @returns {Promise}
    */

  static async postScore (dateScore, points, accuracy, idType, userId) {
    await mySqlStore.connection.awaitQuery(`INSERT INTO Score(dateScore, points, accuracy, ID_User, ID_Type) VALUES
      (?, ?, ?, ?, ?)`, [dateScore, points, accuracy, userId, idType])
    return { dateScore, points, accuracy, userId, idType }
  }
}

module.exports = Score
