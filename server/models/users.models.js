const mySqlStore = require('../mysql-store.js')
const bcrypt = require('bcrypt')

class User {
  /**
   * @summary Function to get all the users from the database.
   * @returns {Promise}
   */
  static async getAllUsers () {
    const result = await mySqlStore.connection.awaitQuery('SELECT id_user, username, pwd FROM Users')
    return result
  }

  /**
    * @summary Function to get a user by id.
    * @param {number} id Id of the user to get.
    * @returns {Promise}
    */
  static async getUser (id) {
    const result = await mySqlStore.connection.awaitQuery('SELECT ID_User, username, pwd FROM Users WHERE ID_User=?', [id])
    return result
  }

  // /**
  //   * @summary Function to delete a user by id.
  //   * @param {number} id Id of the user we want to delete.
  //   * @returns {Promise}
  //   */
  // static async deleteUser (id) {
  //   const result = await mySqlStore.connection.awaitQuery('DELETE FROM Users WHERE ID_User=?', [id])
  //   return result
  // }

  // /**
  //   * @summary Function to update a user.
  //   * @param {number} id
  //   * @param {string} username
  //   * @param {string} password
  //   */
  // static async putUser (id, username, password) {
  //   const result = await mySqlStore.connection.awaitQuery('UPDATE Users SET username=?, pwd=? WHERE ID_User=?', [username, password, id])
  //   return result
  // }

  static async authenticateUser (username, pwd) {
    const result = await mySqlStore.connection.awaitQuery('SELECT * FROM Users WHERE username = ?', [username])
    if (result.length > 0) {
      const response = await bcrypt.compare(pwd, result[0].pwd)
      if (response) {
        // Auth réussi
        const userId = result[0].ID_User
        return ({ response, userId, username, pwd })
      } else {
        // Si le mot de passe est mauvais
        return ({ error: 'Mauvais Mot de passe !' })
      }
    } else {
      // Si l'utilisateur n'existe pas
      return ({ error: 'L\'utilisateur n\'existe pas' })
    }
  }

  /**
    * @summary Function to create a new user.
    * @param {string} username  New username.
    * @param {string} pwd  New password.
    * @param {string} mail New email
    * @returns {Promise}
    */

  static async registerUser (username, pwd, mail) {
    const saltRounds = 10
    // On utilise le bcrypt pour crypter notre mot de passe afin d'améliorer la sécurité des comptes utilisateurs.
    const hashPassword = await new Promise((resolve, reject) => {
      bcrypt.hash(pwd, saltRounds, (err, hash) => {
        if (err) {
          reject(new Error(`${err}`))
        } else {
          resolve(hash)
        }
      })
    })
    // Une fois le hashage terminé, on insère dans notre base de données avec notre login
    const result = await mySqlStore.connection.awaitQuery(
      'INSERT INTO Users(username, pwd, mail) VALUES (?, ?, ?)',
      [username, hashPassword, mail])
    if (result.affectedRows === 1) {
      // Une fois le hashage terminé, on insère dans notre base de données avec notre login
      const resultId = await mySqlStore.connection.awaitQuery('SELECT ID_User FROM Users WHERE username = ?', [username])
      return { userId: resultId[0].ID_User, new_user: username, password: pwd, mail: mail }
    }
  }

  /**
   *
   * @param {number} id
   */
  static async getMe (id) {
    const result = await mySqlStore.connection.awaitQuery('SELECT ID_User, username FROM Users WHERE ID_User=?', [id])
    return result
  }
}

module.exports = User
