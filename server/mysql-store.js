const mysql = require('mysql-await')

class MySqlStore {
  async init (config) {
    this.connection = await mysql.createConnection({
      host: config.host,
      user: config.user,
      password: config.password,
      database: config.database
    })
  }
}

module.exports = new MySqlStore()
