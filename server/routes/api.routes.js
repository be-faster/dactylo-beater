const router = require('express').Router()
const userRoute = require('./users.routes.js')
const dictionaryRoute = require('./dictionary.routes.js')
const scoreRoute = require('./score.routes.js')
const relationshipRoute = require('./friends.routes.js')

/**
 * Router for the configuration of each route
 */

router.use('/', userRoute)
router.use('/', dictionaryRoute)
router.use('/', scoreRoute)
router.use('/', relationshipRoute)

module.exports = router
