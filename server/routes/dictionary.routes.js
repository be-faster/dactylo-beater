const router = require('express').Router()
const dictionaryGetController = require('../controllers/dictionary/get.words.controllers.js')
const dictionaryPostController = require('../controllers/dictionary/post.words.controllers.js')
const errors = require('../middlewares/errors')
// Configuration of dictionary Route

router.get('/wordsSerie', errors(dictionaryGetController.getWordsSerie))

router.post('/word', errors(dictionaryPostController.postWord))

module.exports = router
