const router = require('express').Router()
const friendsGetController = require('../controllers/friends/get.friends.controllers.js')
const friendsPostController = require('../controllers/friends/post.friends.controllers.js')
const isConnected = require('../middlewares/is-connected')
const errors = require('../middlewares/errors')
// Configuration of relationship Route

router.get('/friends', isConnected, errors(friendsGetController.getFriends))
router.get('/friends-high-score', isConnected, errors(friendsGetController.getFriendsHighScore))
router.get('/friends-high-accuracy', isConnected, errors(friendsGetController.getFriendsHighAccuracy))

router.get('/friend/:idFriend', isConnected, errors(friendsGetController.getFriend))

router.post('/friends', isConnected, errors(friendsPostController.postRelationship))

module.exports = router
