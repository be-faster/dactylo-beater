const router = require('express').Router()
const gameTypeGetController = require('../controllers/gameType/get.gameType.controllers.js')
const gameTypePostController = require('../controllers/gameType/post.gameType.controllers.js')
const gameTypeDeleteController = require('../controllers/gameType/delete.gameType.controllers.js')
const isConnected = require('../middlewares/is-connected')

// Configuration of gametype Route

router.get('/gameType', isConnected, gameTypeGetController.getAllGame)
router.get('/gameType/:id', isConnected, gameTypeGetController.getIdGame)

router.post('/gameType/:id/:name', isConnected, gameTypePostController.postGameType)

router.delete('/gameType/:id/:name', isConnected, gameTypeDeleteController.deleteGameType)

module.exports = router
