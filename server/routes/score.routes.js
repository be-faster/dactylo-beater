const router = require('express').Router()
const scoreGetController = require('../controllers/score/get.userscore.controllers')
const scoreGethighScoreController = require('../controllers/score/get.highscore.controllers.js')
const scoreGethighAccuracyController = require('../controllers/score/get.highaccuracy.controllers.js')
const scorePostController = require('../controllers/score/post.score.controllers.js')
const isConnected = require('../middlewares/is-connected')
const errors = require('../middlewares/errors')
// Configuration of score Route

router.get('/high-score-point/', isConnected, errors(scoreGethighScoreController.getHighScorePoint))
router.get('/high-score-accuracy/', isConnected, errors(scoreGethighAccuracyController.getHighScoreAccuracy))
router.get('/score_user', isConnected, errors(scoreGetController.getScoreFromUser))

router.post('/score', isConnected, errors(scorePostController.postScore))

module.exports = router
