const router = require('express').Router()
const userGetController = require('../controllers/users/get.users.controllers.js')
const userPostController = require('../controllers/users/post.users.controllers.js')
const isConnected = require('../middlewares/is-connected')
const error = require('../middlewares/errors')
// Configuration of users Route

router.get('/users', isConnected, error(userGetController.getUsers))
router.get('/user/:id', isConnected, error(userGetController.getUser))
router.get('/me', isConnected, error(userGetController.getMe))

router.post('/login', error(userPostController.authenticateUser))
router.post('/register', error(userPostController.registerUser))
router.post('/logout', isConnected, error(userPostController.logoutUser))

module.exports = router
