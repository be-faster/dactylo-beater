const express = require('express')
const app = express()
const session = require('express-session')

const mySqlStore = require('./mysql-store')
const config = require('./server.config')
const apiRouter = require('./routes/api.routes')
const http = require('http').createServer(app)
const https = require('https')

// @ts-ignore
const io = require('socket.io')(http, {
  cors: {
    origin: '*'
  }
})

const port = process.env.PORT || 3002
const cors = require('cors')
let listUsers = []
const tabWords = []

mySqlStore.init(config.mysql)

app.use(express.json())
app.use(express.urlencoded({ extended: false }))

io.on('connection', socket => {
  socket.on('player-join', ({ username }) => {
    let condition = false
    listUsers.forEach((e) => {
      if (e.username === username) {
        condition = true
      }
    })

    if (condition) {
      io.emit('player-join', listUsers)
      console.log('true', listUsers)
    } else {
      listUsers.push({ username: username, isReady: false, score: 0, inputWord: '' })
      io.emit('player-join', listUsers)
      console.log('false', listUsers)
    }
  })

  socket.on('player-ready', ({ username }) => {
    let autorisationWord = false
    listUsers.forEach((e) => {
      if (e.username === username) {
        e.isReady = true
      }
    })

    if (isAllReady()) {
      autorisationWord = true
      io.emit('player-ready', autorisationWord)
    }
  })
  socket.on('update-score', (score, username) => {
    let endGame = false
    listUsers.forEach((e) => {
      if (e.username === username) {
        e.score = score
        if (e.score >= 100) {
          endGame = true
        }
      }
      if (endGame) {
        listUsers = []
        e.score = 0
      }
    })
    io.emit('update-score', listUsers, endGame)
  })

  https.get('https://dactylobeater.website/api/wordsSerie', (resp) => {
    resp.on('data', (chunk) => {
      const tmp = JSON.parse(chunk.toString())
      for (const i of tmp) {
        tabWords.push(i)
      }
    })

    resp.on('end', () => {
      socket.on('random-word', ({ wantWord }) => {
        if (wantWord) {
          const randomWordSocket = tabWords[Math.floor(Math.random() * tabWords.length)]
          io.emit('random-word', randomWordSocket)
          console.log('rand', randomWordSocket)
        }
      })
    })
  }).on('error', (err) => {
    console.log('Error : ' + err.message)
  })

  socket.on('manual-disconnect', () => {
    console.log('User disconnected')
    listUsers = []
  })
})

const isAllReady = () => {
  let isReady = true
  console.log('tab user size :', listUsers.length)
  if (listUsers.length === 1) {
    isReady = false
  } else {
    listUsers.forEach((e) => {
      console.log(e.username, ' ', e.isReady)
      if (e.isReady === false) isReady = false
    })
  }
  console.log('isAllReady', isReady)
  return isReady
}

app.use(
  session({
    secret: 'mysecret',
    resave: false,
    saveUninitialized: false,
    proxy: true
    // cookie: {
    //   sameSite: 'none',
    //   secure: true
    // }
  }))

app.use(cors({
  // origin: 'https://master.d2klb8lvyvw0d8.amplifyapp.com',
  origin: 'http://localhost:3000',
  // origin: 'https://dactylobeater.herokuapp.com',
  credentials: true
}))

app.use('/api/', apiRouter)

http.listen(port, () => {
  console.log('Express Server is up ! On Port : ', port)
})
