/*let existingConf
try {
  existingConf = require('./local.server.config.js')
} catch (err) {
  existingConf = {}
}*/

function e (param) {
  return process.env[param] || '' // || existingConf[param]
}

module.exports.SESSION_SECRET = e('SESSION_SECRET')
module.exports.mysql = {
  user: e('MYSQL_USER'),
  host: e('MYSQL_HOST'),
  database: e('MYSQL_DB'),
  password: e('MYSQL_PASSWORD')
}
