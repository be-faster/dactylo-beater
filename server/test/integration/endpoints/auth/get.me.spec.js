// /* eslint-disable no-unused-expressions */
import { expect } from 'chai'
import pre from '../../pre-post/pre'
import params from '../../test.config'

describe('GET /me', () => {
  before(() => pre.createBlankDatabaseWithEmptyTables(params))

  it('should respond with the current user when authenticated', async () => {
    const { agent, user } = await pre.createExpressUserAndLogUserIn()
    agent
      .get('/api/me')
      .expect('Content-Type', /json/)
      .expect(200)
      .then((response) => {
        expect(response.body[0].username).to.equal(user.new_user)
        expect(response.body[0].password, '').to.be.undefined
      })
  })
  it('should respond with 401 error when not authenticated', async () => {
    const { agent } = await pre.createExpress()
    agent
      .get('/api/me')
      .expect('Content-Type', /json/)
      .expect(401)
  })
})
