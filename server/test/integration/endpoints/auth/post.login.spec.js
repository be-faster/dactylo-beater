// /* eslint-disable no-unused-expressions */
import { expect } from 'chai'
import { response } from 'express'
import pre from '../../pre-post/pre'
import params from '../../test.config'

describe('POST /login', () => {
  beforeEach(() => pre.createBlankDatabaseWithEmptyTables(params))

  it('should respond with the current user if authentication succeeded', async () => {
    const { agent, user } = await pre.createExpressUser()
    agent.post('/api/login')
      .send({ username: user.new_user, password: user.password })
      .expect('Content-Type', /json/)
      .expect(200)
      .then((response) => {
        expect(response.body.username).to.equal(user.new_user)
        expect(response.body.password, '').to.be.undefined
      })
  })

  it('should respond with status 401 if authentication failed with bad password', async () => {
    const { agent, user } = await pre.createExpressUser()
    agent
      .post('/api/login')
      .send({ username: user.new_user, password: '' })
      .expect('Content-Type', /json/)
      .expect(401)
      .then((response) => {
        expect(response.body.error).to.equal('Mauvais Mot de passe !')
      })
  })

  it('sshould respond with status 401 if authentication failed with bad username', async () => {
    const { agent, user } = await pre.createExpressUser()
    agent
      .post('/api/login')
      .send({ username: '', password: user.password })
      .expect('Content-Type', /json/)
      .expect(401)
      .then((response) => {
        expect(response.body.error).to.equal('L\'utilisateur n\'existe pas')
      })
  })

  it('should not set session userId if authentication already done', async () => {
    const { agent, user } = await pre.createExpressUserAndLogUserIn()
    agent
      .post('/api/login')
      .send({ username: user.new_user, password: user.password })
      .expect('Content-Type', /json/)
      .expect(200)
      .then((response) => {
        expect(response.body.username).to.equal(user.new_user)
        expect(response.body.password, '').to.be.undefined
      })
  })
})
