// /* eslint-disable no-unused-expressions */
import pre from '../../pre-post/pre'
import params from '../../test.config'

describe('POST /logout', () => {
  let user, agent
  before(() => pre.createBlankDatabaseWithEmptyTables(params))
  beforeEach(async () => {
    const ret = await pre.createExpressUser()
    user = ret.user
    agent = ret.agent
  })

  it('should logout the user if authenticated', async () => {
    await pre.loginUser({ agent, user })
    return agent
      .post('/api/logout')
      .expect(200)
  })
  it('should send 401 error if the user is not logged in', async () => {
    return agent
      .post('/api/logout')
      .expect(401)
  })
})
