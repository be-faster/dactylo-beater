// /* eslint-disable no-unused-expressions */
import { expect } from 'chai'
import pre from '../../pre-post/pre'
import params from '../../test.config'

describe('GET /wordsSerie', () => {
  before(() => pre.createBlankDatabaseWithEmptyTables(params))

  it('should respond with a serie of 200 words when authenticated', async () => {
    const { agent } = await pre.createExpressUserAndLogUserGetWordsSerie()
    agent
      .get('/api/wordsSerie')
      .expect('Content-Type', /json/)
      .expect(200)
      .then((response) => {
        expect(response.body).to.be.an('array').of.length(200)
      })
  })
  it('should respond with 401 error when not authenticated', async () => {
    const { agent } = await pre.createExpress()
    agent
      .get('/api/wordsSerie')
      .expect('Content-Type', /json/)
      .expect(401)
  })
})
