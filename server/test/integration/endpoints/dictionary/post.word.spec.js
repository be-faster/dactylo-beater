// /* eslint-disable no-unused-expressions */
import { expect } from 'chai'
import pre from '../../pre-post/pre'
import params from '../../test.config'

describe('POST /word', () => {
  before(() => pre.createBlankDatabaseWithEmptyTables(params))

  it('should respond with the new word when authenticated', async () => {
    const { agent } = await pre.createExpressUserAndLogUserIn()
    agent
      .post('/api/word')
      .send({ word: 'wordTest', point: 2 })
      .expect('Content-Type', /json/)
      .expect(200)
      .then((response) => {
        expect(response.body).to.be.an('object')
        expect(response.body.word).to.equal('wordTest')
        expect(response.body.point).to.equal(2)
      })
  })
  it('should respond with 401 error when not authenticated', async () => {
    const { agent } = await pre.createExpress()
    agent
      .get('/api/word')
      .expect('Content-Type', /json/)
      .expect(401)
  })
})
