import { expect } from 'chai'
import pre from '../../pre-post/pre'
import params from '../../test.config'

describe('GET /api/friend', async () => {
  before(() => pre.createBlankDatabaseWithEmptyTables(params))

  it('should return the user friend by ID if he is connected', async () => {
    const { agent, userFriend, user } = await pre.createExpressUserAndLogUserInAddFriend()
    return agent
      .get('/api/friend/' + userFriend.userId)
      .expect('Content-Type', /json/)
      .expect(200)
      .then((response) => {
        expect(response.body).to.be.an('array').of.length(1)
        expect(response.body[0].ID_User1).to.equal(user.userId)
        expect(response.body[0].ID_User2).to.equal(userFriend.userId)
      })
  })

  it('should return the message error if he is not your friend', async () => {
    const { agent } = await pre.createExpressUserAndLogUserIn()
    return agent
      .get('/api/friend/1')
      .expect('Content-Type', /json/)
      .expect(200)
      .then((response) => {
        expect(response.body).to.be.an('object')
        expect(response.body.Message).to.equal('Not in your friends list')
      })
  })

  it('should return 401 if the user is not connected', async () => {
    const { agent } = await pre.createExpress()
    return agent
      .get('/api/friend/2')
      .expect(401)
  })
})
