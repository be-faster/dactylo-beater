import { expect } from 'chai'
import pre from '../../pre-post/pre'
import params from '../../test.config'

describe('GET /api/friends-high-accuracy', async () => {
  before(() => pre.createBlankDatabaseWithEmptyTables(params))

  it('should return all user\'s friends high-accuracy if he is connected', async () => {
    const { agent } = await pre.createExpressUserAndLogUserGetFriendsHighAccuracy()
    return agent
      .get('/api/friends-high-accuracy')
      .expect('Content-Type', /json/)
      .expect(200)
      .then((response) => {
        expect(response.body.friendsHighAccuracy).to.be.an('array').of.length(1)
        expect(response.body.friendsHighAccuracy[0].highAccuracy).to.equal(10)
      })
  })

  it('should return all user\'s friends Username and ID if he is connected but never played', async () => {
    const { agent } = await pre.createExpressUserAndLogUserInAddFriend()
    return agent
      .get('/api/friends-high-accuracy')
      .expect('Content-Type', /json/)
      .expect(200)
      .then((response) => {
        expect(response.body.friendsHighAccuracy).to.be.an('array').of.length(1)
        expect(response.body.friendsHighAccuracy[0].highAccuracy).to.be.undefined
      })
  })

  it('should return 401 if the user is not connected', async () => {
    const { agent } = await pre.createExpress()
    return agent
      .get('/api/friends-high-accuracy')
      .expect(401)
  })
})
