import { expect } from 'chai'
import pre from '../../pre-post/pre'
import params from '../../test.config'

describe('GET /api/friends-high-score', async () => {
  before(() => pre.createBlankDatabaseWithEmptyTables(params))

  it('should return all user\'s friends high-score if he is connected', async () => {
    const { agent } = await pre.createExpressUserAndLogUserGetFriendsHighScore()
    return agent
      .get('/api/friends-high-score')
      .expect('Content-Type', /json/)
      .expect(200)
      .then((response) => {
        expect(response.body.friendsHighScore).to.be.an('array').of.length(1)
        expect(response.body.friendsHighScore[0].highPoint).to.equal(10)
      })
  })

  it('should return all user\'s friends Username and ID if he is connected but never played', async () => {
    const { agent } = await pre.createExpressUserAndLogUserInAddFriend()
    return agent
      .get('/api/friends-high-score')
      .expect('Content-Type', /json/)
      .expect(200)
      .then((response) => {
        expect(response.body.friendsHighScore).to.be.an('array').of.length(1)
        expect(response.body.friendsHighScore[0].highPoint).to.be.undefined
      })
  })

  it('should return 401 if the user is not connected', async () => {
    const { agent } = await pre.createExpress()
    return agent
      .get('/api/friends-high-score')
      .expect(401)
  })
})
