import { expect } from 'chai'
import pre from '../../pre-post/pre'
import params from '../../test.config'

describe('GET /api/friends', async () => {
  before(() => pre.createBlankDatabaseWithEmptyTables(params))

  it('should return all user\'s friends if he is connected', async () => {
    const { agent, userFriend } = await pre.createExpressUserAndLogUserInAddFriend()
    return agent
      .get('/api/friends')
      .expect('Content-Type', /json/)
      .expect(200)
      .then((response) => {
        expect(response.body).to.be.an('array').of.length(1)
        expect(response.body[0].username).to.equal(userFriend.new_user)
      })
  })

  it('should return 401 if the user is not connected', async () => {
    const { agent } = await pre.createExpress()
    return agent
      .get('/api/friends')
      .expect(401)
  })
})
