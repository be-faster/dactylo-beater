import { expect } from 'chai'
import pre from '../../pre-post/pre'
import params from '../../test.config'

describe('POST /api/friends', async () => {
  beforeEach(() => pre.createBlankDatabaseWithEmptyTables(params))
  it('should add a new friend when connected', async () => {
    const { agent, user } = await pre.createExpressUserAndLogUserIn()
    const user2 = await pre.createUser()
    const user3 = await pre.createUser()
    return agent
      .post('/api/friends')
      .send({ usernameFriend: user2.user.new_user })
      .expect('Content-Type', /json/)
      .expect(200)
      .then((response) => {
        expect(response.body.userId).to.equal(user.userId)
        expect(response.body.usernameFriend).to.equal(user2.user.new_user)
      })
  })

  it('should add a new friend when connected', async () => {
    const { agent, user } = await pre.createExpressUserAndLogUserIn()
    const user2 = await pre.createUser()
    return agent
      .post('/api/friends')
      .send({ usernameFriend: user2.user.new_user })
      .expect('Content-Type', /json/)
      .expect(200)
      .then((response) => {
        expect(response.body.userId).to.equal(user.userId)
        expect(response.body.usernameFriend).to.equal(user2.user.new_user)
      })
  })

  it('should return a 401 error when not connected', async () => {
    const { agent } = await pre.createExpress()
    const user = await pre.createUser()
    return agent
      .get('/api/friends')
      .send({ usernameFriend: user.user.new_user })
      .expect('Content-Type', /json/)
      .expect(401)
  })
})
