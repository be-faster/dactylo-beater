import { expect } from 'chai'
import pre from '../../pre-post/pre'
import params from '../../test.config'

describe('GET /api/high-score-accuracy/', async () => {
  beforeEach(() => pre.createBlankDatabaseWithEmptyTables(params))

  it('should return user\'s highscore if he is connected', async () => {
    const { agent } = await pre.createExpressUserAndLogUserGetFriendsHighAccuracy()
    return agent
      .get('/api/high-score-accuracy/')
      .expect('Content-Type', /json/)
      .expect(200)
      .then((response) => {
        expect(response.body).to.be.an('array').of.length(3)
        expect(response.body[0].accuracy).to.equal(10)
        expect(response.body[1].accuracy).to.equal(8)
        expect(response.body[2].accuracy).to.equal(0)
      })
  })

  it('should return 401 if the user is not connected', async () => {
    const { agent } = await pre.createExpress()
    return agent
      .get('/api/high-score-point/')
      .expect(401)
  })
})
