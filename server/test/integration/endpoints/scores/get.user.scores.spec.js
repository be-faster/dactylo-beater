import { expect } from 'chai'
import pre from '../../pre-post/pre'
import params from '../../test.config'

describe('GET /api/score_user', async () => {
  before(() => pre.createBlankDatabaseWithEmptyTables(params))

  it('should return all user\'s scores', async () => {
    const { agent } = await pre.createExpressUserAndLogUserInPostUserScore()
    return agent
      .get('/api/score_user')
      .expect('Content-Type', /json/)
      .expect(200)
      .then((response) => {
        expect(response.body).to.be.an('array').of.length(2)
        expect(response.body[0].points).to.equal(10)
        expect(response.body[0].accuracy).to.equal(0)
        expect(response.body[1].points).to.equal(8)
        expect(response.body[1].accuracy).to.equal(0)
      })
  })

  it('should return 401 if the user is not connected', async () => {
    const { agent } = await pre.createExpress()
    return agent
      .get('/api/score_user')
      .expect(401)
  })
})
