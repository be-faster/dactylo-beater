import { expect } from 'chai'
import pre from '../../pre-post/pre'
import params from '../../test.config'

describe('POST /api/score', async () => {
  beforeEach(() => pre.createBlankDatabaseWithEmptyTables(params))
  it('should add a new score when connected', async () => {
    const { agent } = await pre.createExpressUserAndLogUserIn()
    return agent
      .post('/api/score')
      .send({ dateScore: new Date('2020-11-18 00:00:00'), points: 10, accuracy: 5.0, idType: 1 })
      .expect('Content-Type', /json/)
      .expect(200)
      .then((response) => {
        expect(response.body.points).to.equal(10)
        expect(response.body.accuracy).to.equal(5)
      })
  })

  it('should return a 401 error when not connected', async () => {
    const { agent } = await pre.createExpress()
    return agent
      .post('/api/score')
      .send({ dateScore: new Date('2020-11-18 00:00:00'), points: 10, accuracy: 5.0, idType: 1 })
      .expect('Content-Type', /json/)
      .expect(401)
  })
})
