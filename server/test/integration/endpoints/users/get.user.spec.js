import { expect } from 'chai'
import pre from '../../pre-post/pre'
import params from '../../test.config'

describe('GET /api/user/:id', async () => {
  beforeEach(() => pre.createBlankDatabaseWithEmptyTables(params))
  it('should return an existing user by his ID when we are connected', async () => {
    const { agent } = await pre.createExpressUserAndLogUserIn()
    const user2 = await pre.createUser()
    return agent
      .get('/api/user/' + user2.user.userId)
      .expect('Content-Type', /json/)
      .expect(200)
      .then((response) => {
        expect(response.body).to.be.an('array').of.length(1)
        expect(response.body[0].username).to.equal(user2.user.new_user)
      })
  })

  it('should return a 401 error when not connected', async () => {
    const { agent } = await pre.createExpress()
    const user = await pre.createUser()
    return agent
      .get('/api/user/' + user.user.userId)
      .expect('Content-Type', /json/)
      .expect(401)
  })
})
