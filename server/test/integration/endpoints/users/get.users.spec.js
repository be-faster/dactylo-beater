import { expect } from 'chai'
import pre from '../../pre-post/pre'
import params from '../../test.config'

describe('GET /api/users', async () => {
  beforeEach(() => pre.createBlankDatabaseWithEmptyTables(params))
  it('should return all existing users if he is connected', async () => {
    const { agent, user } = await pre.createExpressUserAndLogUserIn()
    // console.log(agent.get('/api/users').then((response) => console.log(response)))
    return agent
      .get('/api/users')
      .expect('Content-Type', /json/)
      .expect(200)
      .then((response) => {
        expect(response.body).to.be.an('array').of.length(1)
        expect(response.body[0].username).to.equal(user.new_user)
      })
  })

  it('should return 401 if the user is not connected', async () => {
    const { agent } = await pre.createExpressUser()
    return agent
      .get('/api/users')
      .expect(401)
  })
})
