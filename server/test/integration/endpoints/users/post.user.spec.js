import { expect } from 'chai'
import pre from '../../pre-post/pre'
import params from '../../test.config'

describe('POST /api/register', async () => {
  beforeEach(() => pre.createBlankDatabaseWithEmptyTables(params))
  it('should return the new user when created', async () => {
    const { agent } = await pre.createExpress()

    return agent
      .post('/api/register')
      .send({ username: 'user1', password: 'fakepassword', mail: 'user1@hotmail.fr' })
      .expect('Content-Type', /json/)
      .expect(200)
      .then((response) => {
        expect(response.body).to.be.an('object')
        expect(response.body.new_user).to.be.equal('user1')
        expect(response.body.mail).to.be.equal('user1@hotmail.fr')
        expect(response.body.password).to.be.undefined
      })
  })
})
