const asyncPipe = require('../../../utils/async-pipe')
const expressSession = require('express-session')
const router = require('../../../routes/api.routes')
const mySqlStore = require('../../../mysql-store')
const request = require('supertest')
const express = require('express')
const deleteTables = require('../../../utils/delete-tables')
const randomService = require('../../../utils/random.service')
class Pre {
  /**
   * @param {{ user: String, password: String, database: String, host: String }} params
   */
  static async createBlankDatabaseWithEmptyTables (params) {
    await mySqlStore.init(params)
    await deleteTables()
  }

  static createExpress (params = {}) {
    const app = express()
    const appSession = expressSession({
      secret: 'testing',
      resave: true,
      saveUninitialized: true
    })
    app.use(appSession)
    app.use(express.json())
    app.use(express.urlencoded({ extended: false }))
    app.use('/api/', router)
    return { appSession, agent: request.agent(app), ...params }
  }

  static async createUser (params = {}) {
    const User = require('../../../models/users.models')
    const random = randomService.generateRandomString()
    const user = await User.registerUser(
      'FAKEUSER' + random,
      'fakepassword',
      random + '@email.com'
    )
    return { user, ...params }
  }

  static async createFriend (params = {}) {
    const User = require('../../../models/users.models')
    const random = randomService.generateRandomString()
    const userFriend = await User.registerUser(
      'FAKEFRIEND' + random,
      'fakefriendpassword',
      random + '@email.com'
    )
    return { userFriend, ...params }
  }

  static async createSecondFriend (params = {}) {
    const User = require('../../../models/users.models')
    const random = randomService.generateRandomString()
    const userFriend2 = await User.registerUser(
      'FAKEFRIEND2' + random,
      'fakefriend2password',
      random + '@email.com'
    )
    return { userFriend2, ...params }
  }

  static async loginUser ({ agent, user, ...params }) {
    await agent.post('/api/login')
      .send({ username: user.new_user, password: user.password })
    return { agent, user, ...params }
  }

  static async postUser2Score ({ agent, user, ...params }) {
    const Score = require('../../../models/score.models')
    const score1User = await Score.postScore(new Date('2020-11-18 00:00:00'), 10, 0.0, 1, user.userId)
    const score2User = await Score.postScore(new Date('2020-11-18 00:00:00'), 8, 0.0, 1, user.userId)
    return { agent, user, score1User, score2User, ...params }
  }

  static async postFriendScore ({ agent, userFriend, ...params }) {
    const Score = require('../../../models/score.models')
    const scoreFriend = await Score.postScore(new Date('2020-11-18 00:00:00'), 0, 0.0, 1, userFriend.userId)
    return { agent, userFriend, scoreFriend, ...params }
  }

  static async postFriend3Scores ({ agent, userFriend, ...params }) {
    const Score = require('../../../models/score.models')
    const scoreFriend = await Score.postScore(new Date('2020-11-18 00:00:00'), 0, 0.0, 1, userFriend.userId)
    const score2Friend = await Score.postScore(new Date('2020-11-18 00:00:00'), 10, 0.0, 1, userFriend.userId)
    const score3Friend = await Score.postScore(new Date('2020-11-18 00:00:00'), 8, 0.0, 1, userFriend.userId)
    return { agent, userFriend, scoreFriend, score2Friend, score3Friend, ...params }
  }

  static async postFriend3Accuracy ({ agent, userFriend, ...params }) {
    const Score = require('../../../models/score.models')
    const scoreFriend = await Score.postScore(new Date('2020-11-18 00:00:00'), 0, 8.0, 1, userFriend.userId)
    const score2Friend = await Score.postScore(new Date('2020-11-18 00:00:00'), 0, 10.0, 1, userFriend.userId)
    const score3Friend = await Score.postScore(new Date('2020-11-18 00:00:00'), 0, 0.0, 1, userFriend.userId)
    return { agent, userFriend, scoreFriend, score2Friend, score3Friend, ...params }
  }

  static async postSecondFriendScore ({ agent, userFriend2, ...params }) {
    const Score = require('../../../models/score.models')
    const scoreFriend2 = await Score.postScore(new Date('2020-11-18 00:00:00'), 10, 0.0, 1, userFriend2.userId)
    return { agent, userFriend2, scoreFriend2, ...params }
  }

  static async addFriend ({ agent, user, userFriend, ...params }) {
    await agent.post('/api/friends')
      .send({ user: user.userId, usernameFriend: userFriend.new_user })
    return { agent, user, userFriend, ...params }
  }

  static async addSecondFriend ({ agent, user, userFriend2, ...params }) {
    await agent.post('/api/friends')
      .send({ user: user.userId, usernameFriend: userFriend2.new_user })
    return { agent, user, userFriend2, ...params }
  }

  static async createWords ({ agent, ...params }) {
    for (let i = 0; i <= 200; i++) {
      await agent.post('/api/word')
        .send({ word: randomService.generateRandomString(), point: 2 })
    }
    return { agent, ...params }
  }

  static createExpressUserAndLogUserIn (params = {}) {
    return asyncPipe(
      Pre.createExpress,
      Pre.createUser,
      Pre.loginUser
    )(params)
  }

  static createExpressUserAndLogUserInPostUserScore (params = {}) {
    return asyncPipe(
      Pre.createExpress,
      Pre.createUser,
      Pre.loginUser,
      Pre.postUser2Score
    )(params)
  }

  static createExpressUserAndLogUserInAddFriend (params = {}) {
    return asyncPipe(
      Pre.createExpress,
      Pre.createUser,
      Pre.loginUser,
      Pre.createFriend,
      Pre.addFriend
    )(params)
  }

  // static createExpressUserAndLogUserInAdd2Friends (params = {}) {
  //   return asyncPipe(
  //     Pre.createExpress,
  //     Pre.createUser,
  //     Pre.createFriend,
  //     Pre.addFriend,
  //     Pre.createSecondFriend,
  //     Pre.secondFriendAddUs,
  //     Pre.loginUser
  //   )(params)
  // }

  static createExpressUserAndLogUserGetFriendsHighScore (params = {}) {
    return asyncPipe(
      Pre.createExpress,
      Pre.createUser,
      Pre.loginUser,
      Pre.createFriend,
      Pre.postFriend3Scores,
      Pre.addFriend
    )(params)
  }

  static createExpressUserAndLogUserGetFriendsHighAccuracy (params = {}) {
    return asyncPipe(
      Pre.createExpress,
      Pre.createUser,
      Pre.loginUser,
      Pre.createFriend,
      Pre.postFriend3Accuracy,
      Pre.addFriend
    )(params)
  }

  static createExpressUserAndLogUserGetWordsSerie (params = {}) {
    return asyncPipe(
      Pre.createExpress,
      Pre.createUser,
      Pre.loginUser,
      Pre.createWords
    )(params)
  }

  // static createExpressUserAndLogUserGetHighScore (params = {}) {
  //   return asyncPipe(
  //     Pre.createExpress,
  //     Pre.createUser,
  //     Pre.loginUser,
  //     Pre.createFriend,
  //     Pre.createSecondFriend,
  //     Pre.postFriendScore,
  //     Pre.postSecondFriendScore,
  //     Pre.addFriend,
  //     Pre.addSecondFriend
  //   )(params)
  // }

  static createExpressUser (params = {}) {
    return asyncPipe(
      Pre.createExpress,
      Pre.createUser
    )(params)
  }
}

module.exports = Pre
