const config = require('../../server.config')

module.exports = {
  ...config.mysql,
  database: 'dactylobeaterdbtest'
}
