const mySqlStore = require('../mysql-store')

async function deleteTables_ () {
  const result = await mySqlStore.connection.awaitQuery("SELECT table_name FROM information_schema.tables WHERE table_schema = 'dactylobeaterdbtest';")

  for (const row of result) {
    if (row.TABLE_NAME !== 'GameType') {
      await mySqlStore.connection.awaitQuery(`DELETE from ${row.TABLE_NAME}`)
    }
  }
}

/**
 * @param {{ user: String, password: String, database: String, host: String }}
 */
async function deleteTables () {
  try {
    // Delete all foreign keys
    await deleteTables_()
  } catch (err) {
  }
}

module.exports = deleteTables
