const chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz{}\\/!@#&[]()_$?<>~'

class RandomService {
  /**
   * @param {Number} [length=8]
   * @returns {String}
   */
  static generateRandomString (length = 8) {
    let str = ''
    for (let i = 0; i < length; i++) {
      const rnum = Math.floor(Math.random() * chars.length)
      str += chars.substring(rnum, rnum + 1)
    }
    return str
  }
}

module.exports = RandomService
